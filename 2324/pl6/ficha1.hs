areaQuadrado :: Int -> Int
-- areaQuadrado lado = lado * lado
areaQuadrado lado = lado ^ 2

perimetroRetangulo :: Int -> Int -> Int
perimetroRetangulo comprimento largura = comprimento * 2 + largura * 2

-- d)
remove :: [a] -> [a]
remove lista =
  if listaComprimentoPar lista
    then tail lista
    else init lista

listaComprimentoPar :: [a] -> Bool
listaComprimentoPar lista = mod (length lista) 2 == 0

-- ["Nelson", "Estevão"] -> "N.Estevão"
abreviatura :: [String] -> String
abreviatura lista = (head (head lista)) : '.' : (last lista)
-- abreviatura lista = [(head (head lista))] ++ ['.'] ++ (last lista)
