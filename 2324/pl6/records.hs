import Test.HUnit

data PessoaStandard =
  PessoaStandard String Int
  deriving (Show)

data Curso = Informatica | Economia | Biologia deriving Show

data PessoaRecord =
  PessoaRecord
    { nome :: String
    , idade :: Int
    , curso :: Curso
    }
  deriving (Show)

ps = PessoaStandard "Rui" 18
p1 = PessoaRecord{idade=19, nome="Ines", curso = Informatica}
p2 = PessoaRecord{idade=18, nome="Pedro", curso = Informatica}
p3 = PessoaRecord{idade=23, nome="Matilde", curso = Economia}


eAdulto :: PessoaRecord -> Bool
-- eAdulto pessoa = idade pessoa >= 18
eAdulto PessoaRecord{idade = x} = x >= 18

eAdultoTestes :: Test
eAdultoTestes = test ["quando tem 16 anos" ~: False ~=? eAdulto PessoaRecord{idade = 16, nome="Vasco", curso = Informatica}
    , "quando tem 28 anos" ~: True ~=? eAdulto PessoaRecord{idade= 28, nome = "Tiago", curso= Informatica}
    , "quando tem 18 anos" ~: True ~=? eAdulto PessoaRecord{idade= 18, nome = "Marco", curso= Informatica}
    ]


todosInformatica :: [PessoaRecord] -> Bool
todosInformatica pessoas = all aux pessoas
    where
        aux :: PessoaRecord -> Bool
        aux p = case p of
            PessoaRecord{curso = Informatica} -> True
            PessoaRecord{curso = _} -> False

todosInformaticaTestes :: Test
todosInformaticaTestes = test [ "lista vazia" ~: True ~=? todosInformatica []
    , "todos são de informática" ~: True ~=? todosInformatica [p1, p2]
    , "uma pessoa é de economia" ~: False ~=? todosInformatica [p1, p2, p3]
    ]

main = runTestTTAndExit $ test  [todosInformaticaTestes, eAdultoTestes]