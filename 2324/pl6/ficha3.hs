type Nome = String

type Coordenada = (Int, Int)

data Movimento
  = N
  | S
  | E
  | W
  deriving (Show, Eq) -- norte, sul, este, oeste

type Movimentos = [Movimento]

data PosicaoPessoa =
  Pos Nome Coordenada
  deriving (Show, Eq)

pessoasNorte :: [PosicaoPessoa] -> [Nome]
pessoasNorte [] = []
pessoasNorte l = filtraPessoas l coordenadaMaisNorte
  where
    coordenadaMaisNorte = maior l

filtraPessoas :: [PosicaoPessoa] -> Int -> [Nome]
filtraPessoas [] m = []
filtraPessoas ((Pos nome (x, y)):t) m =
  if y == m
    then nome : filtraPessoas t m
    else filtraPessoas t m

maior :: [PosicaoPessoa] -> Int
maior ((Pos nome (x, y)):t) = maiorAux t y

maiorAux :: [PosicaoPessoa] -> Int -> Int
maiorAux [] m = m
maiorAux ((Pos nome (x, y)):t) m =
  if y > m
    then maiorAux t y
    else maiorAux t m
{-
maior :: [(Int, Int)] -> Int
maior ((a,b):t) = maiorAux t b

maiorAux :: [(Int, Int)] -> Int -> Int
maiorAux ((a,b):t) m = if b > m
    then maiorAux t b
    else maiorAux t m

maior' :: [(Int, Int)] -> Int
maior' l = maior2 $ sndLista l

sndLista :: [(Int, Int)] -> [Int]
sndLista (h:t) = snd h : sndLista t

maior2 :: [Int] -> Int
maior2 (h:t) = maior2aux t h

maior2' :: [Int] -> Int
maior2' [x] = x
maior2' (a:b:t) = if a > b 
    then maior2' (a:t)
    else maior2' (b:t)

maior2aux :: [Int] -> Int -> Int
maior2aux [] m = m
maior2aux (h:t) m = if h > m then maior2aux t h else maior2aux t m
-}
