{-| Module  : Ficha4
Description : Testes Unitários em Haskell
Copyright   : Nelson Estevão <d12733@di.uminho.pt>

Ficha 4 de Laboratórios de Informática I para o ano letivo 2023/24.
-}
module Ficha4 where

avaliacao :: [Double] -> Double -> Int
avaliacao notaQuestoes notaProjeto =
  round $ 0.4 * (sum notaQuestoes) + 0.6 * notaProjeto

-- 3
trocaPrimeiroComUltimo :: [a] -> [a]
trocaPrimeiroComUltimo [] = []
trocaPrimeiroComUltimo [x] = [x]
trocaPrimeiroComUltimo lista = [ultimo] ++ meio ++ [cabeca]
  where
    ultimo = last lista
    meio = init $ tail lista
    cabeca = head lista

trocaPrimeiroComUltimoNaMatrix :: [[a]] -> [[a]]
trocaPrimeiroComUltimoNaMatrix [] = []
trocaPrimeiroComUltimoNaMatrix (h:t) =
  trocaPrimeiroComUltimo h : trocaPrimeiroComUltimoNaMatrix t

minhaMatriz = ["ABF", "CDP", "N42"]
