module MyFibSpec where

import MyFib
import Test.HUnit

testesFib :: Test
testesFib =
  test
    [ "Para n = 1" ~: 1 ~=? fib 1
    , "Para n = 0" ~: 1 ~=? fib 0
    , "Para n = 5" ~: 8 ~=? fib 5
    ]

testesMysum :: Test
testesMysum =
  test
    [ "100 + 50" ~: 150 ~=? mysum 100 50
    , "10 + (-10)" ~: 0 ~=? mysum 10 (-10)
    , "(-1) + (-5)" ~: -6 ~=? mysum (-1) (-5)
    ]

main = runTestTTAndExit $ test [testesFib, testesMysum]
