module Ficha4Spec where

import Ficha4
import Test.HUnit

avaliacaoTestes =
  test
    [ "Quando se têm 20 a tudo" ~: 20 ~=?
      avaliacao [2, 2, 2, 2, 2, 2, 2, 2, 2, 2] 20
    , "Quando se têm 10 a tudo" ~: 10 ~=?
      avaliacao [1, 1, 1, 1, 1, 1, 1, 1, 1, 1] 10
    , "Quando se têm 10 nas questoes e 20 no projeto" ~: 16 ~=?
      avaliacao [1, 1, 1, 1, 1, 1, 1, 1, 1, 1] 20
    , "Quando se têm 15 nas questoes e 16 no projeto" ~: 16 ~=?
      avaliacao [1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5] 16
    ]

main = runTestTTAndExit avaliacaoTestes
