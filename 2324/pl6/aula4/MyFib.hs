module MyFib where

{- |
Função que calcula o n-ésimo elemento da sequência de Fibonnaci.
-}
fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

mysum :: Int -> Int -> Int
mysum x y = x + y
