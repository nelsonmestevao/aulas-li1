module Ficha2 where

data Movimento
  = Norte
  | Sul
  | Este
  | Oeste
  deriving (Show)

type Ponto = (Double, Double)

move :: Ponto -> Movimento -> Ponto
-- move (x, y) Norte = (x, y +1)
-- move (x, y) Sul = (x, y -1)
-- move (x, y) Este = (x + 1, y)
-- move (x, y) Oeste = (x - 1, y)
move (x, y) movimento =
  case movimento of
    Norte -> (x, y + 1)
    Sul -> (x, y - 1)
    Este -> (x + 1, y)
    Oeste -> (x - 1, y)

dist :: Ponto -> Ponto -> Double
dist (x1, y1) (x2, y2) = sqrt $ (x2 - x1) ^ 2 + (y2 - y1) ^ 2

data Figura
  = Circulo Ponto Double
  | Rectangulo Ponto Ponto
  deriving (Show)

pertence :: Ponto -> Figura -> Bool
pertence (x, y) (Rectangulo (px, py) (mx, my)) =
  x >= px && x <= mx && y >= py && y <= my
-- pertence ponto (Circulo centro raio) =
--   let distancia = dist ponto centro
--    in distancia <= raio
-- pertence ponto (Circulo centro raio) =
--     dist ponto centro <= raio
pertence ponto (Circulo centro raio) = distancia <= raio
  where
    distancia = dist ponto centro

{-
Comentário
Multi-linha
-}
data Ponto2
  = Cartesiano Double Double
  | Polar Double Double
  deriving (Show, Eq)

posx :: Ponto2 -> Double
posx (Cartesiano x _) = x
posx (Polar r theta) = r * cos theta

posy :: Ponto2 -> Double
posy (Cartesiano _ y) = y
posy (Polar r theta) = r * sin theta
