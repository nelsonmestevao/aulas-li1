{-|
Module      : Exemplo
Description : Módulo Haskell contendo exemplos de funções recursivas
Copyright   : (c) Nelson Estevão <d12733@di.uminho.p>;

Este módulo contém definições Haskell para o cálculo de funções
recursivas simples (obs: isto é apenas uma descrição mais
longa do módulo para efeitos de documentação...).
-}
module Exemplo where

{-| A função 'fib' retorna o @n@-ésimo elemento da sequência de Fibonacci.

== Exemplos de utilização:

>>> fib 4
3
>>> fib 10
55
-}
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)
