e :: Int
e = 1

filtraStrings :: [String] -> Char -> [String]
filtraStrings palavras c = filter (\s -> aux s c) palavras
  where
    aux :: String -> Char -> Bool
    aux palavra c = c /= head palavra

adicionaLista :: [(Int, Int)] -> Int -> [(Int, Int)]
adicionaLista lista n = map (\(x, y) -> (x + n, y)) lista
  where
    aux :: Int -> (Int, Int) -> (Int, Int)
    aux n (x, y) = (x + n, y)

comprimento :: [Int] -> Int
comprimento lista = foldr (proximo) (0) lista
  where
    proximo :: Int -> Int -> Int
    proximo a = succ

somaPrimeiro :: [(Int, Int)] -> Int
-- somaPrimeiro lista = sum $ map fst lista
-- somaPrimeiro lista = foldr (soma) 0 lista
somaPrimeiro lista = foldr (curry $ \((x, y), b) -> x + b) 0 lista
  where
    soma :: (Int, Int) -> Int -> Int
    soma (x, y) n = x + n
