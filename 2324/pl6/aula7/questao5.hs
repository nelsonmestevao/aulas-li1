import Test.HUnit

f :: String -> Maybe String
f frase =
  if tamanho <= 1
    then Nothing
    else Just $ unwords $ inicio ++ ["##"] ++ fim
  where
    palavras = words frase
    tamanho = length palavras
    meio = div tamanho 2
    -- (inicio, fim) = splitAt palavras meio
    inicio = take meio palavras
    fim = drop meio palavras

f_testes =
  test
    [ "Teste com frase impar" ~: Just "Hoje ## chove muito" ~=?
      f "Hoje chove muito"
    , "Teste com unica palavra" ~: Nothing ~=? f "Chove"
    , "Teste com string vazia" ~: Nothing ~=? f ""
    , "Teste com frase par" ~: Just "Hoje chove ## muito mesmo" ~=?
      f "Hoje chove muito mesmo"
    ]

main = runTestTTAndExit f_testes
