module Main where

import Data.Maybe
import System.Random

-- estev.ao/li2324
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

type Coordenada = (Float, Float)

data Orientação
  = Norte
  | Sul
  | Este
  | Oeste
  deriving (Show, Eq, Ord)

data Cobra =
  Cobra Orientação [Coordenada]
  deriving (Show, Eq, Ord)

type Maçã = Coordenada

data Imagem
  = Apple
  | Snake
  deriving (Eq, Show)

data Jogo =
  Jogo Cobra Maçã

type Imagens = [(Imagem, Picture)]

-- data TyposImagemCobra = HeadUp | HeadDown | HeadLeft | HeadRight 
-- type ImagensCobra = [(TyposImagemCobra, Picture)]
-- type TyposImagemMaca = MacaVermelha | MacaDourada
-- type ImagensMaca = [(TyposImagemMaca, Picture)]
-- type Estado = (Jogo, (ImagensMaca, ImagensCobra))
type Estado = (Jogo, Imagens)

getImagem :: Imagem -> Imagens -> Picture
getImagem key dicionario = fromJust $ lookup key dicionario

janela :: Display
janela = InWindow "Jogo da Cobra (versão da Wish)" (600, 600) (300, 150)

corFundo :: Color
corFundo = white

frameRate :: Int
frameRate = 10

estadoInicial :: Jogo
estadoInicial = Jogo (Cobra Sul [(20, 0), (20, 10), (20, 20)]) (200, 70)

desenhaMaca :: Imagens -> Maçã -> Picture
desenhaMaca imgs (x, y) = Translate x y $ Scale 0.5 0.5 imagem
  where
    imagem = getImagem Apple imgs

desenhaCobra :: Cobra -> Picture
desenhaCobra (Cobra orientacao posicoes) = Pictures [cabeca, corpo]
  where
    (x, y) = head posicoes
    cabeca = Translate x y $ Color orange $ rectangleSolid 10 10
    corpo = desenhaCorpoCobra (tail posicoes)

desenhaCorpoCobra :: [Coordenada] -> Picture
desenhaCorpoCobra posicoes = Pictures $ map desenhaBloco posicoes
  where
    desenhaBloco (x, y) = Translate x y $ Color green $ rectangleSolid 10 10

desenha :: Estado -> IO Picture
desenha (Jogo cobra maca, imgs) =
  return $ Pictures $ [desenhaCobra cobra, desenhaMaca imgs maca]

reage :: Event -> Estado -> IO Estado
reage (EventKey (SpecialKey KeyUp) Down _ _) (Jogo (Cobra orientacao posicoes) maca, imgs) =
  return $ (Jogo (Cobra Norte posicoes) maca, imgs)
reage (EventKey (SpecialKey KeyDown) Down _ _) (Jogo (Cobra orientacao posicoes) maca, imgs) =
  return $ (Jogo (Cobra Sul posicoes) maca, imgs)
reage (EventKey (SpecialKey KeyLeft) Down _ _) (Jogo (Cobra orientacao posicoes) maca, imgs) =
  return $ (Jogo (Cobra Oeste posicoes) maca, imgs)
reage (EventKey (SpecialKey KeyRight) Down _ _) (Jogo (Cobra orientacao posicoes) maca, imgs) =
  return $ (Jogo (Cobra Este posicoes) maca, imgs)
reage _ e = return e

tempo :: Float -> Estado -> IO Estado
tempo t (Jogo (Cobra orientacao posicoes) maca, imgs) = do
  x <- randomRIO (0, 29)
  y <- randomRIO (0, 29)
  let p = par x y
  n <- putStrLn (show p)
  if head novaPosicaoCobra == maca
    then return
           ( Jogo
               (Cobra orientacao (adicionaCabeca orientacao novaPosicaoCobra))
               (p)
           , imgs)
    else return (Jogo (Cobra orientacao novaPosicaoCobra) maca, imgs)
  where
    par :: Float -> Float -> (Float, Float)
    par x y =
      ( fromIntegral $ ((floor x) * 10) :: Float
      , fromIntegral $ ((floor y) * 10) :: Float)
    novaPosicaoCobra = moveCobra orientacao posicoes
    adicionaCabeca Norte ((x, y):t) = (x, y + 10) : (x, y) : t
    adicionaCabeca Sul ((x, y):t) = (x, y - 10) : (x, y) : t
    adicionaCabeca Este ((x, y):t) = (x + 10, y) : (x, y) : t
    adicionaCabeca Oeste ((x, y):t) = (x - 10, y) : (x, y) : t

moveCobra :: Orientação -> [Coordenada] -> [Coordenada]
moveCobra Norte ((x, y):t) = (x, y + 10) : (x, y) : init t
moveCobra Sul ((x, y):t) = (x, y - 10) : (x, y) : init t
moveCobra Este ((x, y):t) = (x + 10, y) : (x, y) : init t
moveCobra Oeste ((x, y):t) = (x - 10, y) : (x, y) : init t

carregarImagens :: IO Imagens
carregarImagens = do
  apple <- loadBMP "apple.bmp"
  head_up <- loadBMP "head_up.bmp"
  return $ [(Apple, apple), (Snake, head_up)]

main = do
  imgs <- carregarImagens
  playIO janela corFundo frameRate (estadoInicial, imgs) desenha reage tempo
