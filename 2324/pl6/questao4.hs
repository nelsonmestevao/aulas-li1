type ListaCompras = [(Produto, Quantidade)]

type Produto = String

type Quantidade = Int

type PrecosProdutos = [(Produto, Preco)]

type Preco = Int

{-|
== Exemplos

>>> f [("XXX", 1),("AAA", 3),("YYY", 1)] [("AAA", 10),("CCC", 5),("XXX", 15),("YYY", 10)]
55
-}
f :: ListaCompras -> PrecosProdutos -> Int
f lc pp = sum $ aux lc pp

aux :: ListaCompras -> PrecosProdutos -> [Int]
aux lc pp = map (multi pp) lc
  where
    multi :: PrecosProdutos -> (Produto, Quantidade) -> Int
    multi precos (p, q) = q * encontraPreco p precos

encontraPreco :: Produto -> PrecosProdutos -> Int
encontraPreco produto precos =
  case lookup produto precos of
    Nothing -> 0
    Just preco -> preco
-- encontraPreco p [] = 0
-- encontraPreco p ((produto, preco):pcs) =
--   if p == produto
--     then preco
--     else encontraPreco p pcs
