import System.Random

geraAleatorio :: IO Int
geraAleatorio = do
  let y = 2
  x <- randomRIO (1, 10)

  return $ x * y