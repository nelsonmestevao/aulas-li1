module Main where

import Data.Maybe
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import System.Directory.Internal.Prelude (exitFailure)
import System.Random

type Coordenadas = (Float, Float)

data Direção
  = Norte
  | Sul
  | Este
  | Oeste
  deriving (Show, Eq, Ord)

data Cobra
  = Cobra Direção [Coordenadas]

type Maçã = (Coordenadas, TiposMaca)

data Jogo
  = Jogo Cobra Maçã

data TiposMaca
  = Vermelha
  | Dourada
  deriving (Show, Eq)

data Imagem
  = Apple
  | GoldenApple
  | Snake
  deriving (Show, Eq)

type Imagens = [(Imagem, Picture)]

data MenuInicialOpcoes = Jogar | Sair deriving (Show, Eq)

data Modo = MenuInicial MenuInicialOpcoes | EmJogo deriving (Show, Eq)

-- type Estado = (Jogo, Imagens)
data Estado = Estado
  { jogo :: Jogo,
    imagens :: Imagens,
    modo :: Modo
  }

getImagem :: Imagem -> Imagens -> Picture
getImagem k d = fromJust $ lookup k d

jogoInicial :: Jogo
jogoInicial =
  Jogo (Cobra Norte [(20, 20), (20, 10), (20, 0)]) ((70, 20), Vermelha)

opcaoJogar = Translate (-150) (100) $ Text "Jogar"

opcaoSair = Translate (-150) (-100) $ Text "Sair"

desenha :: Estado -> IO Picture
desenha e@Estado {modo = MenuInicial Jogar} =
  return $ Pictures [Color blue opcaoJogar, opcaoSair]
desenha e@Estado {modo = MenuInicial Sair} =
  return $ Pictures [opcaoJogar, Color blue opcaoSair]
desenha e@Estado {jogo = Jogo cobra maca, imagens = imgs} =
  return $ Pictures [desenhaMaca imgs maca, desenhaCobra cobra]

desenhaCobra :: Cobra -> Picture
desenhaCobra (Cobra dir pos) =
  Pictures $
    (Color orange $ head blocos) : (map (\p -> Color green p) $ tail blocos)
  where
    blocos = map desenhaBloco pos
    desenhaBloco :: Coordenadas -> Picture
    desenhaBloco (x, y) = Translate x y $ rectangleSolid 10 10

desenhaMaca :: Imagens -> Maçã -> Picture
desenhaMaca imgs ((x, y), t) =
  Translate x y $ Scale 0.5 0.5 $ getImagem key imgs
  where
    key =
      case t of
        Vermelha -> Apple
        Dourada -> GoldenApple

reage :: Event -> Estado -> IO Estado
reage (EventKey (SpecialKey KeyEsc) Down _ _) e@Estado {modo = EmJogo} =
  return e {modo = MenuInicial Jogar}
reage (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {modo = EmJogo} =
  return e {jogo = movimenta Norte (jogo e)}
reage (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {modo = EmJogo} =
  return e {jogo = movimenta Sul (jogo e)}
reage (EventKey (SpecialKey KeyLeft) Down _ _) e@Estado {modo = EmJogo} =
  return e {jogo = movimenta Oeste (jogo e)}
reage (EventKey (SpecialKey KeyRight) Down _ _) e@Estado {modo = EmJogo} =
  return e {jogo = movimenta Este (jogo e)}
reage (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {modo = MenuInicial Jogar} =
  return e {modo = MenuInicial Sair}
reage (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {modo = MenuInicial Sair} =
  return e {modo = MenuInicial Jogar}
reage (EventKey (SpecialKey KeyEnter) Down _ _) e@Estado {modo = MenuInicial Jogar} =
  return e {modo = EmJogo}
reage (EventKey (SpecialKey KeyEnter) Down _ _) e@Estado {modo = MenuInicial Sair} = exitFailure
reage _ e = return e

movimenta :: Direção -> Jogo -> Jogo
movimenta d (Jogo (Cobra dir pos) (maca, tipo)) = Jogo (Cobra d pos) (maca, tipo)

tempo :: Float -> Estado -> IO Estado
tempo t e@Estado {jogo = Jogo cobra (maca, tipo), modo = EmJogo} = do
  x <- randomRIO (5.0, 40.0) :: IO Float
  y <- randomRIO (5.0, 40.0) :: IO Float
  k <- randomRIO (1, 100) :: IO Float
  putStrLn (show (x, y))
  let novoTipo =
        if k < 50.0
          then Vermelha
          else Dourada
  let (a, b) = (10 * floor x, 10 * floor y)
  putStrLn (show (a, b))
  let novaMaca =
        if comeuMaca
          then ((fromInteger a :: Float, fromInteger b :: Float), novoTipo)
          else (maca, tipo)
  return e {jogo = Jogo novaCobra novaMaca}
  where
    cabeca = cabecaCobra novaPosicoes
    novaPosicoes = moveCobra cobra
    comeuMaca = cabeca == maca
    novaCobra =
      if comeuMaca
        then adicionaBlocoCobra novaPosicoes (ultimoCobra cobra)
        else novaPosicoes
tempo t e = return e

ultimoCobra :: Cobra -> Coordenadas
ultimoCobra (Cobra dir pos) = last pos

cabecaCobra (Cobra dir pos) = head pos

adicionaBlocoCobra :: Cobra -> Coordenadas -> Cobra
adicionaBlocoCobra (Cobra dir pos) p = (Cobra dir (pos ++ [p]))

moveCobra :: Cobra -> Cobra
moveCobra (Cobra dir ((x, y) : t)) =
  case dir of
    Norte -> Cobra Norte $ (x, y + 10) : (x, y) : (init t)
    Sul -> Cobra Sul $ (x, y - 10) : (x, y) : (init t)
    Este -> Cobra Este $ (x + 10, y) : (x, y) : (init t)
    Oeste -> Cobra Oeste $ (x - 10, y) : (x, y) : (init t)

janela :: Display
janela =
  InWindow
    "Jogo da Cobra (versão da wish)" -- título da janela
    (650, 650) -- dimensão da janela
    (500, 200) -- posição no ecrã

corFundo :: Color
corFundo = greyN 1.0

frameRate :: Int
frameRate = 10

carregarImagens :: IO Imagens
carregarImagens = do
  apple <- loadBMP "apple.bmp"
  apple2 <- loadBMP "apple2.bmp"
  snake <- loadBMP "head_up.bmp"
  let imgs = [(Apple, apple), (Snake, snake), (GoldenApple, apple2)]
  return imgs

main = do
  imgs <- carregarImagens
  playIO janela corFundo frameRate (Estado {jogo = jogoInicial, imagens = imgs, modo = MenuInicial Jogar}) desenha reage tempo
