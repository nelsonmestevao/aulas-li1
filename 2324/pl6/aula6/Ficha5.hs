contaP :: String -> String -> Int
contaP frase palavra = contaPaux 0 (words frase) palavra

contaPaux :: Int -> [String] -> String -> Int
contaPaux i [] palavra = i
contaPaux i (h:t) palavra =
  if h == palavra
    then contaPaux (i + 1) t palavra
    else contaPaux i t palavra
