module Main where

import Graphics.Gloss

circulo1 :: Picture
circulo1 = Circle 50

circulo2 :: Picture
circulo2 = Translate (-60) 30 circulo1

circuloVermelho = Color red circulo1

circuloAzul = Color blue circulo2

poligonoAmarelo =
  scale 2 2 $
  Translate (-25) (-25) $
  Color yellow $ Polygon [(0, 0), (0, 50), (50, 50), (50, 0)]

circulos = Pictures [circuloVermelho, circuloAzul, poligonoAmarelo]

window :: Display
window =
  InWindow
    "Janela de Exemplo" -- título da janela 
    (350, 350) -- dimensão da janela  
    (850, 200) -- posição no ecrã 

background :: Color
--background = yellow -- greyN 1.0 
background = makeColorI 211 211 211 255

main :: IO ()
main = display window background circulos
