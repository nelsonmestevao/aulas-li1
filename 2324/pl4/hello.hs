data Semaforo
  = Vermelho
  | Amarelo
  | Verde
  deriving (Show, Eq)
-- instance Show Semaforo where
--     show :: Semaforo -> String
--     show Vermelho = "🔴"
--     show Amarelo = "🟡"
--     show Verde = "🟢"
-- instance Eq Semaforo where
--     (==) Vermelho Vermelho = True
--     (==) Vermelho Amarelo = True
--     (==) Vermelho Verde = False
