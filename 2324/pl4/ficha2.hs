data Movimento
  = Norte
  | Sul
  | Este
  | Oeste
  deriving (Show)

type Ponto = (Double, Double)

move :: Ponto -> Movimento -> Ponto
move (x, y) movimento =
  case movimento of
    Norte -> (x, y + 1)
    Sul -> (x, y - 1)
    Este -> (x + 1, y)
    Oeste -> (x - 1, y)

dist :: Ponto -> Ponto -> Double
dist (x1, y1) (x2, y2) = sqrt $ (x2 - x1) ^ 2 + (y2 - y1) ^ 2

type Velocidade = (Double, Double)

type Tempo = Double

move2 :: Ponto -> Velocidade -> Tempo -> Ponto
move2 (x, y) (vx, vy) t = (x + vx * t, y + vy * t)
