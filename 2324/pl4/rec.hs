type Nome = String

type Coordenada = (Double, Double)

data Movimento
  = N
  | S
  | E
  | W
  deriving (Show, Eq) -- norte, sul, este, oeste

type Movimentos = [Movimento]

data PosicaoPessoa =
  Pos Nome Coordenada
  deriving (Show, Eq)

move :: PosicaoPessoa -> Movimento -> PosicaoPessoa
move (Pos nome (x, y)) N = Pos nome (x, y + 1)
move (Pos nome (x, y)) S = Pos nome (x, y - 1)
move (Pos nome (x, y)) E = Pos nome (x + 1, y)
move (Pos nome (x, y)) W = Pos nome (x - 1, y)

posicao :: PosicaoPessoa -> [Movimento] -> PosicaoPessoa
posicao p@(Pos nome (x, y)) [] = p
posicao p@(Pos nome (x, y)) (m:ms) = posicao (move p m) ms
