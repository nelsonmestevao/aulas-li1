module MyFibSpec where

import Test.HUnit

import MyFib

fibTests :: Test
fibTests =
  test
    [ "Para n = 0" ~: 1 ~=? fib 0
    , "Para n = 1" ~: 1 ~=? fib 1
    , "Para n = 5" ~: 8 ~=? fib 5
    ]

main = runTestTTAndExit fibTests
