{- |
Module      : Ficha4
Description : Testes Unitários em Haskell
Copyright   : Nelson Estevão <d12733@di.uminho.pt>

Testes Unitários em Haskell usando HUnit.
-}
module Ficha4 where

import Test.HUnit

-- 3
trocaColunas :: [a] -> [a]
trocaColunas [] = []
trocaColunas [x] = [x]
trocaColunas lista = [ultimo] ++ meio ++ [cabeça]
  where
    cabeça = head lista
    ultimo = last lista
    meio = init (tail lista)
        -- meio = tail $ init lista

-- | Troca a primeira coluna com a ultima para todas as linhas da matriz
trocaColunasEmMatriz :: [[a]] -> [[a]]
trocaColunasEmMatriz [] = []
trocaColunasEmMatriz (h:t) = trocaColunas h : trocaColunasEmMatriz t

trocaColunasEmMatrizTests :: Test
trocaColunasEmMatrizTests =
  test
    [ "Quando passamos uma matriz de Char" ~: ["cba", "1il", "hfp"] ~=?
      trocaColunasEmMatriz ["abc", "li1", "pfh"]
    ]

-- 4
{- | Função recursiva que procure a posição de um elemento numa lista (posição
da primeira ocorrência). Devolve (-1) caso o elemento não ocorra na lista. O índice de
posições começa em zero.

== Exemplos

>>> procura "ABCB" 'B'
1

>>> procura [1,3,4,1] 1
0
-}
procura :: Eq a => [a] -> a -> Int
procura = procuraAux 0

procuraAux :: Eq a => Int -> [a] -> a -> Int
procuraAux _ [] _ = -1
procuraAux i (h:t) a
  | h == a = i
  | otherwise = procuraAux (i + 1) t a

procuraTests :: Test
procuraTests =
  test
    [ "procura B em ABCB" ~: 1 ~=? procura "ABCB" 'B'
    , "procura 1 [1,3,4,1]" ~: 0 ~=? procura [1, 3, 4, 1] 1
    ]

main = runTestTTAndExit $ test [procuraTests, trocaColunasEmMatrizTests]
