{- |
Module      : MyFib
Description : Sequência de Fibonnaci e utilitários
Copyright   : Nelson Estevão <d12733@di.uminho.pt>

Este módulo apenas exporta uma única função, 'fib' que implementa a sequência de Fibonnaci.
-}
module MyFib where

{- |
Sequência de Fibonnaci.

== Exemplos

>>> fib 1
1
>>> fib 5
8
-}
fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)
