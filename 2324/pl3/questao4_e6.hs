module Questao4 where

type Produto = String

type Preco = Int

type Quantidade = Int

type ListaCompras = [(Produto, Quantidade)]

type PrecosProdutos = [(Produto, Preco)]

{-|
== Exemplos
>>> f [("XXX",1),("AAA",3),("YYY",1)] [("AAA",10),("CCC",5),("XXX",15),("YYY", 10)]
55
-}
f :: ListaCompras -> PrecosProdutos -> Int
f lc dpp = foldr (\(p, q) i -> q * (encontra p dpp) + i) 0 lc

f2 :: ListaCompras -> PrecosProdutos -> Int
f2 lc dpp = foldr (aux dpp) 0 lc
  where
    aux :: [(Produto, Preco)] -> (Produto, Quantidade) -> Int -> Int
    aux dppreco (p, q) i = q * (encontra p dppreco) + i

f3 :: ListaCompras -> PrecosProdutos -> Int
f3 lc dpp = sum $ geraCustos lc dpp

f3' :: ListaCompras -> PrecosProdutos -> Int
f3' = sum . geraCustos

geraCustos :: ListaCompras -> PrecosProdutos -> [Int]
geraCustos lc dpp = map (\(p, q) -> q * encontra p dpp) lc

geraCustos' [] dpp = []
geraCustos' ((p, q):t) dpp = q * encontra p dpp : geraCustos t dpp

encontra :: Produto -> PrecosProdutos -> Preco
encontra p dpp =
  case lookup p dpp of
    Nothing -> 0
    Just preco -> preco

encontra' :: Produto -> PrecosProdutos -> Preco
encontra' p [] = 0
encontra' p ((produto, preco):t) =
  if p == produto
    then preco
    else encontra p t
