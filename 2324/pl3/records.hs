import Test.HUnit
import GHC.Data.UnionFind (Link(Info))

data Pessoa = Pessoa String Int deriving Show

data Curso = Informatica | Biologia | Economia deriving (Eq, Show)

data Estudante = Estudante{nome :: String, idade :: Int, curso :: Curso} deriving (Eq, Show)

p1 :: Pessoa
p1 = Pessoa "Rita" 19
p2 = Pessoa "Marco" 18

e1 = Estudante{nome = "Rita", idade = 19, curso = Informatica}

e2 = Estudante{idade = 18, nome="Marco", curso = Informatica}

e3 = e1 {nome = "Pedro", idade = 20}

e4 = e3 {curso = Biologia}

envelhece :: Estudante -> Estudante
envelhece e = e {idade = succ $ idade e}

envelheceTestes :: Test
envelheceTestes = test ["quando tem 19 anos" ~: Estudante{nome = "Rita", idade=20, curso=Informatica} ~=? envelhece e1]

envelheceEstudantes :: [Estudante] -> [Estudante]
envelheceEstudantes es = map envelhece es

{-|
Função que filtra estudantes de cursos que não são Informática.

== Exemplos

>>> soInformatica [e1, e2, e3]
[e1, e2, e3]

>>> soInformatica [e2, e4]
[e2]
-}
soInformatica :: [Estudante] -> [Estudante]
soInformatica es = filter aux es
    where
        aux :: Estudante -> Bool
        aux e = case e of
            Estudante{curso = Informatica} -> True
            Estudante{curso = _} -> False

soInformaticaTestes :: Test
soInformaticaTestes = test 
    [ "lista vazia" ~: [] ~=? soInformatica []
    , "quando só há alunos de informática" ~: [e1, e2] ~=? soInformatica [e1, e2]
    , "quando há alunos de Biologia " ~: [e1, e2] ~=? soInformatica [e1, e2, e4]
    , "quando só há alunos de outros cursos " ~: [] ~=? soInformatica [e4]
    ]


main = runTestTTAndExit $ test [soInformaticaTestes, envelheceTestes]