hello = "Hello, World!"

ePar :: Int -> Bool
ePar x = mod x 2 == 0

verifica :: Char -> String -> Bool
verifica c palavra = elem c palavra

-- d) Defina uma função que recebe uma lista não vazia e que remove o primeiro elemento
-- da lista, se ela tiver um número par de elementos. Se a lista tiver um número impar
-- de elementos, remove o último elemento
remove :: [a] -> [a]
remove lista =
  if mod (length lista) 2 == 0
    then tail lista
    else init lista

constroiPar :: [a] -> (a, a)
constroiPar lista = (head lista, last lista)

parnomes :: [String] -> (String, String)
-- nomes lista = constroiPar lista
parnomes = constroiPar

head1Lista2 :: ([a], [b]) -> (a, [b])
head1Lista2 (xs, ys) = (head xs, ys)

abreviatura :: [String] -> String
abreviatura nomes = head (head nomes) : '.' : last nomes
--abreviatura nomes = [head (head nomes)] ++ ['.'] ++ last nomes
