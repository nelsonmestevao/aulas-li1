{-|
Module      : Aula3
Description : Exercícios da Aula 3 de Laboratórios de Informática I
Copyright   : (c) Nelson Estevão <d12733@di.uminho.pt>;

Terceira aula de LI1 para o ano letivo de 2023/24.
-}
module Aula3 where

-- | Função recursiva que recebe uma lista de inteiros e adiciona um valor dado a cada elemento da lista.
adicionaUnidade :: [Int] -> [Int]
adicionaUnidade [] = []
adicionaUnidade (h:t) = (h + 1) : adicionaUnidade t

-- | Função que recebe uma lista de Strings e remove todas as Strings iniciadas por um dado caractere.
removeC :: [String] -> Char -> [String]
removeC [] _ = []
removeC (h:t) c =
  if head h == c
    then restoFiltrado
    else h : restoFiltrado
  where
    restoFiltrado = removeC t c

-- | Função recursiva que recebe uma lista de pares de inteiros e adiciona um valor dado à primeira componente de cada par.
adicionaUnidadeFstPar :: [(Int, Int)] -> Int -> [(Int, Int)]
adicionaUnidadeFstPar [] _ = []
adicionaUnidadeFstPar ((a, b):ps) x = (a + x, b) : adicionaUnidadeFstPar ps x

mappeiaSnd :: [(Int, Int)] -> [Int]
mappeiaSnd [] = []
-- mappeiaSnd ((a,b):t) = b:mappeiaSnd t
mappeiaSnd (h:t) = (snd h) : mappeiaSnd t

-- | Função recursiva que recebe uma lista, não vazia, de pares de inteiros e calcula qual o maior valor da segunda componente.
maxList :: [(Int, Int)] -> Int
maxList l = maxList' $ mappeiaSnd l

maxList'' :: [Int] -> Int
maxList'' (h:t) = maxList''Aux t h

maxList''Aux :: [Int] -> Int -> Int
maxList''Aux [] m = m
maxList''Aux (h:t) m =
  if h > m
    then maxList''Aux t h
    else maxList''Aux t m

maxList' :: [Int] -> Int
maxList' [h] = h
maxList' (h:t) =
  if h > maxList' t
    then h
    else maxList' t

maxListP' :: [(Int, Int)] -> Int
maxListP' [(a, b)] = b
maxListP' ((a, b):t) =
  if b > maxListP' t
    then b
    else maxListP' t

type Nome = String

type Coordenada = (Int, Int)

data Movimento
  = N
  | S
  | E
  | W
  deriving (Show, Eq) -- norte, sul, este, oeste

type Movimentos = [Movimento]

data PosicaoPessoa =
  Pos Nome Coordenada
  deriving (Show, Eq)

l :: [PosicaoPessoa]
l =
  [ Pos "Nelson" (0, 0)
  , Pos "Lucas" (0, 3)
  , Pos "Matilde" (2, 3)
  , Pos "Ines" (1, 2)
  ]

l2 :: [PosicaoPessoa]
l2 =
  [ Pos "Nelson" (0, 3)
  , Pos "Lucas" (0, 3)
  , Pos "Matilde" (2, 3)
  , Pos "Ines" (1, 3)
  ]

l3 :: [PosicaoPessoa]
l3 =
  [ Pos "Nelson" (0, 3)
  , Pos "Lucas" (0, 2)
  , Pos "Matilde" (2, 1)
  , Pos "Ines" (1, 0)
  ]

pessoasNorte :: [PosicaoPessoa] -> [Nome]
pessoasNorte lista = pessoasNorteAux lista (coordenadaMaisNorte lista)

pessoasNorteAux :: [PosicaoPessoa] -> Int -> [Nome]
pessoasNorteAux [] _ = []
pessoasNorteAux lista@(Pos nome (x, y):pos) m =
  if y == m
    then nome : pessoasNorteAux pos m
    else pessoasNorteAux pos m

coordenadaMaisNorte :: [PosicaoPessoa] -> Int
coordenadaMaisNorte [Pos nome (x, y)] = y
-- coordenadaMaisNorte (Pos nome (x, y)):[] = y
coordenadaMaisNorte (Pos nome (x, y):pos) =
  if y > coordenadaMaisNorte pos
    then y
    else coordenadaMaisNorte pos
