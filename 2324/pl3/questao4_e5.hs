module Questao4 where

type Produto = String

type Quantidade = Int

type ListaCompras = [(Produto, Quantidade)]

{-|
>>> f [("XXX", 1),("AAA", 3),("YYY", 1)] ["AAA", "XXX"]
[("YYY",1)]
-}
f :: ListaCompras -> [Produto] -> ListaCompras
-- f lc produtos = filter (\(p, q) -> not (p `elem` produtos)) lc
f lc produtos = filter (aux produtos) lc
    where
        aux :: [Produto] -> (Produto, Quantidade) -> Bool
        aux pds (p, q) = not $ p `elem` pds
