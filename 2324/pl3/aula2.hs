module Aula2 where

type Ponto = (Double, Double)

data Movimento
  = Norte
  | Sul
  | Este
  | Oeste
  deriving (Show)

move :: Ponto -> Movimento -> Double
move (x, y) movimento =
  case movimento of
    Norte -> (x, y + 1)
    Sul -> (x, y - 1)
    Este -> (x + 1, y)
    Oeste -> (x - 1, y)

dist :: Ponto -> Ponto -> Double
dist (x1, y1) (x2, y2) = sqrt $ (x2 - x1) ^ 2 + (y2 - y1) ^ 2

data Ponto2
  = Cartesiano Double Double
  | Polar Double Double
  deriving (Show, Eq)

posx :: Ponto2 -> Double
posx (Cartesiano x _) = x
posx (Polar r theta) = r * cos theta

posy :: Ponto2 -> Double
posy (Cartesiano _ y) = y
posy (Polar r theta) = r * sin theta

-- | Obtem o __angulo__ das /coordenadas polares/ a partir de um 'Ponto2' cartesiano
-- 
raio :: Ponto2 -> Double
raio (Polar r _) = r
raio (Cartesiano x y) = dist (0, 0) (x, y)

-- Obtem o angulo das coordenadas polares a partir de um ponto cartesiano
-- TODO falta tratar do caso de paragem
-- FIXME tem erro quando o ponto esta na origem
angulo :: Ponto2 -> Double
angulo (Polar _ theta) = theta
angulo (Cartesiano x y) = atan2 y x

main = undefined
