filtraStrings :: [String] -> Char -> [String]
filtraStrings l c = filter (aux c) l
  where
    aux :: Char -> String -> Bool
    aux c l = c /= (head l)

-- filtraStrings :: [String] -> Char -> [String]
-- filtraStrings l c = filter (aux) l 
--     where
--         aux :: String -> Bool
--         aux l = c /= (head l)
-- filtraStrings :: [String] -> Char -> [String]
-- filtraStrings l c = filter (\s -> head s /= c) l 
adicionaValor :: Int -> [(Int, Int)] -> [(Int, Int)]
adicionaValor v l = map (\(x, y) -> (x + v, y)) l

soma :: Num a => [a] -> a
soma = foldr (+) 0
