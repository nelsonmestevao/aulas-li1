module Questao5 where

import Data.Maybe
import Test.HUnit

type Nome = String

{-|
Junta uma lista de nomes só com o primeiro e último usando `&` para unir.

== Exemplos

>>> f ["Jose Gomes Pires", "Ana Costa Santos", "Julio Martins Barroso"]
Just "Jose Pires & Ana Santos & Julio Barroso"

>>> f ["Ana Silva Marques"]
Just "Ana Marques"

>>> f []
Nothing

-}
f :: [Nome] -> Maybe String
f [] = Nothing
f l = Just (faux l)

faux :: [Nome] -> String
faux [x] = pEu x
faux (h:t) = unwords $ [pEu h, "&"] ++ [faux t]

pEu :: Nome -> String
pEu nome = unwords [head nomes, last nomes]
  where
    nomes = words nome

f_tests :: Test
f_tests =
  test
    [ "Para uma lista de nomes" ~: Just "Jose Pires & Ana Santos & Julio Barroso" ~=? f ["Jose Gomes Pires", "Ana Costa Santos", "Julio Martins Barroso"]
    , "Para um só nome" ~: Just "Ana Marques" ~=? f ["Ana Silva Marques"]
    , "Para lista vazia" ~: Nothing ~=? f []
    ]

main = runTestTTAndExit f_tests
