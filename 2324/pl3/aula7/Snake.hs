module Main where

import Data.Maybe
import System.Random

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

type Coordenadas = (Float, Float)

data Direção
  = Norte
  | Sul
  | Este
  | Oeste
  deriving (Show, Eq, Ord)

data Cobra =
  Cobra Direção [Coordenadas]

type Maçã = (Coordenadas, TiposMaca)

data Jogo =
  Jogo Cobra Maçã

data TiposMaca
  = Vermelha
  | Dourada
  deriving (Show, Eq)

data Imagem
  = Apple
  | GoldenApple
  | Snake
  deriving (Show, Eq)

type Imagens = [(Imagem, Picture)]

type Estado = (Jogo, Imagens)

getImagem :: Imagem -> Imagens -> Picture
getImagem k d = fromJust $ lookup k d

estadoInicial :: Jogo
estadoInicial =
  Jogo (Cobra Norte [(20, 20), (20, 10), (20, 0)]) ((70, 20), Vermelha)

desenha :: Estado -> IO Picture
desenha (Jogo cobra maca, imgs) =
  return $ Pictures [desenhaMaca imgs maca, desenhaCobra cobra]

desenhaCobra :: Cobra -> Picture
desenhaCobra (Cobra dir pos) =
  Pictures $
  (Color orange $ head blocos) : (map (\p -> Color green p) $ tail blocos)
  where
    blocos = map desenhaBloco pos
    desenhaBloco :: Coordenadas -> Picture
    desenhaBloco (x, y) = Translate x y $ rectangleSolid 10 10

desenhaMaca :: Imagens -> Maçã -> Picture
desenhaMaca imgs ((x, y), t) =
  Translate x y $ Scale 0.5 0.5 $ getImagem key imgs
  where
    key =
      case t of
        Vermelha -> Apple
        Dourada -> GoldenApple

reage :: Event -> Estado -> IO Estado
reage (EventKey (SpecialKey KeyUp) Down _ _) (Jogo (Cobra dir pos) maca, imgs) =
  return (Jogo (Cobra Norte pos) maca, imgs)
reage (EventKey (SpecialKey KeyDown) Down _ _) (Jogo (Cobra dir pos) maca, imgs) =
  return (Jogo (Cobra Sul pos) maca, imgs)
reage (EventKey (SpecialKey KeyLeft) Down _ _) (Jogo (Cobra dir pos) maca, imgs) =
  return (Jogo (Cobra Oeste pos) maca, imgs)
reage (EventKey (SpecialKey KeyRight) Down _ _) (Jogo (Cobra dir pos) maca, imgs) =
  return (Jogo (Cobra Este pos) maca, imgs)
reage _ e = return e

tempo :: Float -> Estado -> IO Estado
tempo t (Jogo cobra (maca, tipo), imgs) = do
  x <- randomRIO (5.0, 40.0) :: IO Float
  y <- randomRIO (5.0, 40.0) :: IO Float
  k <- randomRIO (1, 100) :: IO Float
  putStrLn (show (x, y))
  let novoTipo =
        if k < 50.0
          then Vermelha
          else Dourada
  let (a, b) = (10 * floor x, 10 * floor y)
  putStrLn (show (a, b))
  let novaMaca =
        if comeuMaca
          then ((fromInteger a :: Float, fromInteger b :: Float), novoTipo)
          else (maca, tipo)
  return (Jogo novaCobra novaMaca, imgs)
  where
    cabeca = cabecaCobra novaPosicoes
    novaPosicoes = moveCobra cobra
    comeuMaca = cabeca == maca
    novaCobra =
      if comeuMaca
        then adicionaBlocoCobra novaPosicoes (ultimoCobra cobra)
        else novaPosicoes

ultimoCobra :: Cobra -> Coordenadas
ultimoCobra (Cobra dir pos) = last pos

cabecaCobra (Cobra dir pos) = head pos

adicionaBlocoCobra :: Cobra -> Coordenadas -> Cobra
adicionaBlocoCobra (Cobra dir pos) p = (Cobra dir (pos ++ [p]))

moveCobra :: Cobra -> Cobra
moveCobra (Cobra dir ((x, y):t)) =
  case dir of
    Norte -> Cobra Norte $ (x, y + 10) : (x, y) : (init t)
    Sul -> Cobra Sul $ (x, y - 10) : (x, y) : (init t)
    Este -> Cobra Este $ (x + 10, y) : (x, y) : (init t)
    Oeste -> Cobra Oeste $ (x - 10, y) : (x, y) : (init t)

janela :: Display
janela =
  InWindow
    "Jogo da Cobra (versão da wish)" -- título da janela
    (650, 650) -- dimensão da janela
    (500, 200) -- posição no ecrã

corFundo :: Color
corFundo = greyN 1.0

frameRate :: Int
frameRate = 10

carregarImagens :: IO Imagens
carregarImagens = do
  apple <- loadBMP "apple.bmp"
  apple2 <- loadBMP "apple2.bmp"
  snake <- loadBMP "head_up.bmp"
  let imgs = [(Apple, apple), (Snake, snake), (GoldenApple, apple2)]
  return imgs

main = do
  imgs <- carregarImagens
  playIO janela corFundo frameRate (estadoInicial, imgs) desenha reage tempo
