contaP :: String -> String -> Int
contaP frase palavra = aux (words frase) palavra
-- contaP frase = auxAcc 0 (words frase)

aux :: [String] -> String -> Int
aux [] _ = 0
aux (h:t) palavra
  | h == palavra = 1 + aux t palavra
  | otherwise = aux t palavra

auxAcc :: Int -> [String] -> String -> Int
auxAcc i [] _ = i
auxAcc i (h:t) palavra
  | h == palavra = auxAcc (i + 1) t palavra
  | otherwise = auxAcc i t palavra
