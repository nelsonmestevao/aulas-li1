module Main where

import Graphics.Gloss

vermelho :: Picture
vermelho = Translate (0) (25) $ Color (makeColorI 255 0 0 255) $ circleSolid 50

azul :: Picture
azul = Translate (-25) (-25) $ Color (makeColorI 0 0 255 255) $ circleSolid 50

verde :: Picture
verde = Translate (25) (-25) $ Color (makeColorI 0 255 0 150) $ circleSolid 50

t :: Picture
t = Translate (0) (-100) $ Color (addColors (red) (blue)) $ circleSolid 50

draw :: Picture
draw = Pictures [vermelho, azul, verde]

window :: Display
window =
  InWindow
    "Janela de Exemplo" -- título da janela
    (250, 250) -- dimensão da janela
    (800, 200) -- posição no ecrã

background :: Color
background = greyN 1.0

main :: IO ()
main = display window background draw
