module Main where

import Graphics.Gloss

circulo1 :: Picture
circulo1 = Circle 50

circulo2 :: Picture
circulo2 = Translate (-60) 30 $ circleSolid 50

-- circulo2 = circulo1
quadrado1 :: Picture
quadrado1 =
  Scale 2 2 $
  Translate (-25) (-25) $ Polygon [(0, 0), (0, 50), (50, 50), (50, 0)]

corPreferida :: Color
corPreferida = makeColorI 153 0 10 155

circuloVermelho = Color red circulo1

circuloAzul = Color blue circulo2

quadradoAmarelo = Color yellow quadrado1

circuloCorPreferida = Color corPreferida circulo2

circulos =
  Pictures [circuloAzul, quadradoAmarelo, circuloCorPreferida, circuloVermelho]

window :: Display
window =
  InWindow
    "Janela de Exemplo" -- título da janela
    (250, 250) -- dimensão da janela
    (800, 200) -- posição no ecrã

background :: Color
background = greyN 0.8

main :: IO ()
main = display window background circulos
