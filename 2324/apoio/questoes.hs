{-|
>>> f ["ola", "nelson", "laboratorios", "informatica"]
[("ola","laboratorios"),("nelson","informatica")]

>>> f ["ola", "nelson", "laboratorios"]
[("ola","nelson"),(" -- ","laboratorios")]
-}
f :: [String] -> [(String, String)]
f ls =
  let n = div (length ls) 2
      (l1, l2) = splitAt n ls
   in zip (l1 ++ [" -- "]) l2

r = [((0, 0), (100, 100)), ((10, 0), (20, 30))]

{-|
== Exemplos

>>> f5 "Ana Gomes Silva"
Just "A G Silva"

>>> f5 "Rui"
Nothing

>>> f5 ""
Nothing
-}
f5 :: String -> Maybe String
f5 name =
  if length (words name) <= 1
    then Nothing
    else Just $ abrevia name
  where
    abrevia :: String -> String
    abrevia nome =
      unwords $
      (map (\x -> [head x]) $ init $ words nome) ++ [last (words nome)]

type ListaCompras = [(Produto, Quantidade)]

type Produto = String

type Quantidade = Int

{-|
== Exemplos

>>> f4 [("XXX", 1), ("AAA", 3), ("YYY", 1)] ["AAA", "XXX"]
[("YYY",1)]
-}
f4 :: ListaCompras -> [Produto] -> ListaCompras
f4 [] ps = []
f4 (h:t) ps =
  if elem (fst h) ps
    then f4 t ps
    else h : f4 t ps

-- f4 lc ps = filter (\(p,q) -> not $ p `elem` ps ) lc
data Hora =
  H Int Int
  deriving (Show, Eq)

data Time
  = AM Int Int
  | PM Int Int
  deriving (Show, Eq)

{-|
== Exemplos

>>> f3 [AM 10 00, PM 3 0]
[H 10 0,H 15 0]
-}
f3 :: [Time] -> [Hora]
-- f3 = map time2hora
f3 [] = []
f3 (h:t) = time2hora h : f3 t

time2hora :: Time -> Hora
time2hora (AM h m) = H h m
time2hora (PM h m) = H (h + 12) m
