module Ficha4 where

{- | 2) Função que dada uma matriz não vazia, troca a primeira linha com a última.

== Exemplos

>>> trocaPU ["ola", "adeus"]
["adeus","ola"]

>>> trocaPU [['o', 'l', 'a'], ['a','d','e','u','s']]
["adeus","ola"]

>>> trocaPU [[1,2,3], [4,5,6], [7,8,9]]
[[7,8,9],[4,5,6],[1,2,3]]
-}
trocaPU :: [a] -> [a]
trocaPU [] = []
trocaPU [x] = [x]
trocaPU lista = [ultima] ++ meio ++ [primeira]
  where
    primeira = head lista
    meio = init (tail lista)
    ultima = last lista

{- | Função que recebe uma matriz não vazia e troca a primeira coluna com a última.

== Exemplos

>>> mapTrocaPU ["ola", "adeus"]
["alo","sdeua"]

>>> mapTrocaPU [[1,2,3], [4,5,6], [7,8,9]]
[[3,2,1],[6,5,4],[9,8,7]]
-}
mapTrocaPU :: [[a]] -> [[a]]
mapTrocaPU [] = []
mapTrocaPU (h:t) = trocaPU h : mapTrocaPU t

{- | Função recursiva que procure a posição de um elemento numa lista (posição
da primeira ocorrência). Devolve (-1) caso o elemento não ocorra na lista. O índice de
posições começa em zero.

== Exemplos

>>> procuraP [8,42,4] 42
1

>>> procuraP [42,60] 73
-1
-}
procuraP :: Eq a => [a] -> a -> Int
procuraP lista a = procuraPaux 0 lista a

procuraPaux :: Eq a => Int -> [a] -> a -> Int
procuraPaux i [] a = -1
procuraPaux i (h:t) a
  | h == a = i
  | otherwise = procuraPaux (i + 1) t a

{- | Função recursiva que procure a posição de um elemento numa matriz (par com o
 - índice da coluna e com o índice da linha, da primeira ocorrência que
 - encontrar).

== Exemplos

>>> procuraMatriz [[4,5,2],[42,3,12,42]] 12
Just (2,1)

>>> procuraMatriz [[4,5,2],[42,3,12,42]] 0
Nothing
-}
procuraMatriz :: Eq a => [[a]] -> a -> Maybe (Int, Int)
procuraMatriz matriz valor = procuraMatrizAux 0 matriz valor

procuraMatrizAux :: Eq a => Int -> [[a]] -> a -> Maybe (Int, Int)
procuraMatrizAux j [] a = Nothing
procuraMatrizAux j (h:t) a
  | procuraP h a /= -1 = Just (procuraP h a, j)
  | otherwise = procuraMatrizAux (j + 1) t a

{-|

== Exemplos

>>> procuraMatrizPares [(2,4), (6,8)] 8
1

>>> procuraMatrizPares [(2,4), (8,6)] 8
-1
-}
procuraMatrizPares :: Eq a => [(a, a)] -> a -> Int
-- procuraMatrizPares [] a = -1
procuraMatrizPares l a = procuraMatrizParesAux 0 l a

procuraMatrizParesAux :: Eq a => Int -> [(a, a)] -> a -> Int
procuraMatrizParesAux i [] a = -1
procuraMatrizParesAux i ((x, y):t) a
  | y == a = i
  | otherwise = procuraMatrizParesAux (i + 1) t a
