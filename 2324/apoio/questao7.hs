module Questao7 where

type Matriz = [[Float]]

{-|
== Exemplos

>>> f [[1,2,3],[0,3,5],[2,1,4]] 1 0
1

>>> f [[0,3,0],[5,8,9],[1,3,4]] 3 1
2
-}
f :: Matriz -> Float -> Int -> Int
f m n i = length $ filter (\x -> (x !! i) == n) m

f2 :: Matriz -> Float -> Int -> Int
f2 [] _ _ = 0
f2 mat x n = f' ((tornaMatriz mat) !! n) x

f' :: [Float] -> Float -> Int
f' [] _ = 0
f' (h:t) n
  | h == n = 1 + f' t n
  | otherwise = f' t n

tornaMatriz :: Matriz -> Matriz
tornaMatriz [] = []
tornaMatriz mat = torna mat : tornaMatriz (dropMat mat)

torna :: Matriz -> [Float]
torna [] = []
torna (h:t) = aux : torna t
  where
    aux = head h

dropMat :: Matriz -> Matriz
dropMat [] = []
dropMat (h:t) = drop 1 h : dropMat t
