{- |
Module      : Fact
Description : Função Fatorial
Copyright   : Nelson Estevão <d12733@di.uminho.pt>

Módulo que implementa a função factorial como sendo 'fact'.
-}
module Fact where

{-|
Função fatorial. Apenas funciona para os Naturais incluindo o zero.

== Exemplos

>>> fact 5
120

== Propriedades

prop> fact 0 == 1
prop> fact 1 == 1
-}
fact ::
     Int -- ^ N-esimo da sequência
  -> Int -- ^ resultado da sequência
fact 0 = 1
fact n = n * fact (n - 1)

trocaLista :: [a] -> [a]
trocaLista [] = []
trocaLista [x] = [x]
trocaLista l = [(last l)] ++ (init $ tail l) ++ [(head l)]

trocaMatrix :: [[a]] -> [[a]]
trocaMatrix [] = []
trocaMatrix (h:t) = trocaLista h : trocaMatrix t

proximaVogal :: Char -> Char
proximaVogal 'a' = 'e'
proximaVogal 'e' = 'i'
proximaVogal 'i' = 'o'
proximaVogal 'o' = 'u'
proximaVogal 'u' = 'a'
proximaVogal c = c

trocaString :: [Char] -> [Char]
trocaString [] = []
trocaString (h:t) = proximaVogal h : trocaString t
