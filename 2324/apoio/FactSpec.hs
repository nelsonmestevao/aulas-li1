module FactSpec where

import Fact
import Test.HUnit

factTests :: Test
factTests =
  test
    [ "Para n = 0" ~: 1 ~=? fact 0
    , "Para n = 1" ~: 1 ~=? fact 1
    , "Para n = 5" ~: 120 ~=? fact 5
    ]

main = runTestTT $ test [factTests, factTests]
