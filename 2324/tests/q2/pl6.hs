import Test.Hspec

data Hora =
  H Int Int
  deriving (Show, Eq)

data Time
  = AM Int Int
  | PM Int Int
  deriving (Show, Eq)

f1 :: Time -> Int -> Time
f1 time minutes =
  if hours >= 12
    then (PM (hours - 12) mins)
    else (AM hours mins)
  where
    totalMins = timeToMinutes time + minutes
    hours = (totalMins `div` 60) `mod` 24
    mins = totalMins `mod` 60

timeToMinutes :: Time -> Int
timeToMinutes (AM h m) = h * 60 + m
timeToMinutes (PM h m) = (12 * 60) + h * 60 + m

type Ponto = (Double, Double)

data Figura
  = Circulo Ponto Double
  | Rectangulo Ponto Ponto
  | Quadrado Ponto Double
  deriving (Show, Eq)

f2 :: Figura -> Double -> Figura
f2 (Circulo p r) factor = Circulo p (r * factor)
f2 (Quadrado p l) factor = Quadrado p (l * factor)
f2 (Rectangulo (x1, y1) (x2, y2)) factor =
  Rectangulo (x1, y1) (x1 + comprimento, y1 + altura)
  where
    comprimento = (x2 - x1) * factor
    altura = (y2 - y1) * factor

main :: IO ()
main =
  hspec $ do
    describe "f1" $ do
      it "converts AM to PM" $ do f1 (AM 11 40) 30 `shouldBe` (PM 0 10)
      it "converts PM to AM" $ do f1 (PM 11 40) 30 `shouldBe` (AM 0 10)
      it "does not change from AM" $ do f1 (AM 00 40) 30 `shouldBe` (AM 1 10)
      it "does not change from PM" $ do f1 (PM 00 40) 30 `shouldBe` (PM 1 10)
    describe "f2" $ do
      it "expand circle" $ do
        f2 (Circulo (0, 0) 1) 2 `shouldBe` (Circulo (0, 0) 2)
      it "reduce circle" $ do
        f2 (Circulo (0, 0) 1) 0.5 `shouldBe` (Circulo (0, 0) 0.5)
      it "expand rectangle" $ do
        f2 (Rectangulo (0, 0) (10, 1)) 2 `shouldBe` (Rectangulo (0, 0) (20, 2))
      it "reduce rectangle" $ do
        f2 (Rectangulo (0, 0) (10, 1)) 0.5 `shouldBe`
          (Rectangulo (0, 0) (5, 0.5))
      it "expand square" $ do
        f2 (Quadrado (0, 0) 1) 2 `shouldBe` (Quadrado (0, 0) 2)
      it "reduce square" $ do
        f2 (Quadrado (0, 0) 1) 0.5 `shouldBe` (Quadrado (0, 0) 0.5)
