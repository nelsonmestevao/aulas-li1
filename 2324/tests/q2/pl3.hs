import Test.Hspec

data Hora =
  H Int Int
  deriving (Show, Eq)

f1 :: Hora -> Hora -> Hora
f1 (H h1 m1) (H h2 m2) =
  if m1 <= m2
    then H (h2 - h1) (m2 - m1)
    else H (h2 - h1 - 1) (60 - m1 + m2)

f1' :: Hora -> Hora -> Hora
f1' (H h1 m1) (H h2 m2) = H hora minuto
  where
    totalMinutos = 60 * h1 + m1 - 60 * h2 + m2
    hora = div totalMinutos 60
    minuto = mod totalMinutos 60

f1'' :: Hora -> Hora -> Hora
f1'' h1 h2 = minutes2hour diff
  where
    diff = (hour2minutes h2) - (hour2minutes h1)

hour2minutes :: Hora -> Int
hour2minutes (H h m) = h * 60 + m

minutes2hour :: Int -> Hora
minutes2hour m = H hours minutes
  where
    hours = div m 60
    minutes = mod m 60

type Ponto = (Double, Double)

data Figura
  = Circulo Ponto Double
  | Rectangulo Ponto Ponto
  | Quadrado Ponto Double
  deriving (Show, Eq)

f2 :: Figura -> Figura -> Bool
f2 (Circulo p1 r1) (Circulo p2 r2) = (dist p2 p1) + r2 < r1
f2 (Rectangulo (x1, y1) (x2, y2)) (Rectangulo (k1, z1) (k2, z2)) =
  k1 > x1 && z1 > y1 && k2 < x2 && z2 < y2
f2 (Quadrado (x1, y1) r1) (Quadrado (x2, y2) r2) =
  x1 < x2 && y1 < y2 && (dist (x1, y1) pontoMaior2 < diag1)
  where
    diag1 = sqrt $ r1 ^ 2 + r1 ^ 2
    pontoMaior2 = (x2 + r2, y2 + r2)
f2 _ _ = False

dist :: Ponto -> Ponto -> Double
dist (x1, y1) (x2, y2) = sqrt $ (y2 - y1) ^ 2 + (x2 - x1) ^ 2

main :: IO ()
main =
  hspec $ do
    describe "f1" $ do
      it "subtraction of hours" $ do f1 (H 1 0) (H 2 0) `shouldBe` (H 1 0)
      it "subtraction of minutes" $ do
        f1 (H 13 45) (H 14 15) `shouldBe` (H 0 30)
    describe "f2" $ do
      it "circle inside same center" $ do
        f2 (Circulo (0, 0) 2) (Circulo (0, 0) 1) `shouldBe` True
      it "circle inside different center" $ do
        f2 (Circulo (0, 0) 2) (Circulo (1, 0) 0.5) `shouldBe` True
      it "circle outside different center" $ do
        f2 (Circulo (0, 0) 2) (Circulo (1, 0) 2) `shouldBe` False
