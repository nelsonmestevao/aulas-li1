module Q3.PL6 where

-- | Representação de uma Hora em formato 24h.
data Hora =
  H Int Int -- ^ o primeiro inteiro são as horas e o segundo são os minutos
  deriving (Show, Eq)

-- | Representação alternativa do tipo 'Hora'.
data Time
  = AM Int Int -- ^ AM 0 0 representa 00h00
  | PM Int Int -- ^ PM 0 0 representa 12h00
  deriving (Show, Eq)

{- | Função que converte uma lista de 'Time' numa lista de 'Hora'.

== Exemplo

>>> f1 [AM 4 50, PM 4 50, PM 0 0, AM 0 5]
[H 4 50,H 16 50,H 12 0,H 0 5]

-}
f1 :: [Time] -> [Hora]
f1 [] = []
f1 ((AM h m):t) = H h m : f1 t
f1 ((PM h m):t) = H (h + 12) m : f1 t

{- | Função que converte uma lista de 'Hora' numa lista de 'Time'. Inverso da 'f1'.

== Exemplo

>>> f1' [H 4 50, H 16 50, H 12 0, H 0 5]
[AM 4 50,PM 4 50,PM 0 0,AM 0 5]

-}
f1' :: [Hora] -> [Time]
f1' [] = []
f1' ((H h m):t)
  | h < 12 = AM h m : f1' t
  | otherwise = PM (h - 12) m : f1' t

{- | Função que soma minutos a uma lista de horas.

== Exemplos

>>> f2 [H 4 50, H 16 40, H 12 0, H 23 50] 20
[H 5 10,H 17 0,H 12 20,H 0 10]
-}
f2 :: [Hora] -> Int -> [Hora]
f2 [] _ = []
f2 (h:t) n = (soma h n) : f2 t n
  where
    soma :: Hora -> Int -> Hora
    soma (H h m) n
      | n + m >= 60 = H (mod (h + (div (n + m) 60)) 24) (mod (n + m) 60)
      | otherwise = H h (m + n)
