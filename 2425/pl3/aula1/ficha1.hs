k :: [String] -> String
-- k nomes = take 1 (head nomes) ++ "." ++ last nomes
-- k nomes = head (head nomes) : '.' : (last nomes)
-- k nomes = [head (head nomes)] ++ '.' : (last nomes)
-- k nomes = [head (head nomes)] ++ "." ++ (last nomes)
k (nome : nomes) = head nome : '.' : last nomes
