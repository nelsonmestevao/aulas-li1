module Main where

import Graphics.Gloss

window :: Display
window = InWindow "Exemplo" (1280, 720) (0, 0)

background :: Color
background = rose

images :: Picture
images = Pictures [quadrado, circulo]
  where
    quadrado = Scale (2) (2) $ Rotate (45) $ Translate (-50) (-50) $ Color violet $ Polygon [(0, 0), (100, 0), (100, 100), (0, 100)]
    circulo = Color black $ Circle 50

main = do
  display window background images