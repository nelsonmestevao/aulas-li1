module Snake
  ( atualiza,
    Cobra (..),
    Direcao (..),
    Ponto,
  )
where

type Ponto = (Float, Float)

data Direcao
  = Cima
  | Baixo
  | Esquerda
  | Direita
  deriving (Show, Eq, Ord)

data Cobra = Cobra Direcao [Ponto] deriving (Show, Eq)

-- | Avança o corpo da cobra na direcao atual.
--
-- == Exemplos
--
-- >>> atualiza (Cobra Cima [(0, 0), (0, -1)])
-- Cobra Cima [(0, 1), (0, 0)]
atualiza :: Cobra -> Cobra
atualiza (Cobra direcao pontos) =
  --   Cobra direcao $ map (\p -> atualizaPonto direcao p) pontos
  Cobra direcao $ map (atualizaPonto direcao) pontos

atualizaPonto :: Direcao -> Ponto -> Ponto
atualizaPonto direcao (x, y) = case direcao of
  Cima -> (x, y + 1)
  Baixo -> (x, y - 1)
  Esquerda -> (x - 1, y)
  Direita -> (x + 1, y)