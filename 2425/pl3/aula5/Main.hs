module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Snake

type World = Cobra

quadrado :: Float -> Picture
quadrado l = Polygon $ rectanglePath l l

desenha :: World -> Picture
desenha (Cobra direcao corpo) = Pictures $ map desenhaCelula corpo

desenhaCelula :: Ponto -> Picture
desenhaCelula (x, y) = Color green $ Translate (x * tamanho) (y * tamanho) $ quadrado tamanho
  where
    tamanho = 20

reageEventos :: Event -> World -> World
reageEventos _ w = w

reageTempo :: Float -> World -> World
reageTempo _ cobra = atualiza cobra

window :: Display
window = InWindow "Jogo da Cobrinha" (1280, 720) (0, 0)

background :: Color
background = white

frameRate :: Int
frameRate = 5

estadoInicial :: World
estadoInicial = Cobra Cima [(0, 0), (0, -1)]

main = do
  play window background frameRate estadoInicial desenha reageEventos reageTempo