module Hello where

a = 1

b = 5

duplica :: Maybe Int -> Maybe Int
duplica mx = do
  x <- mx

  --   Just $ 2 * x
  return $ 2 * x
