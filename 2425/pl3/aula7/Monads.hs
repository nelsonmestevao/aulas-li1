import System.Random

leFicheiro = readFile "hello.txt"

duplica :: Maybe Int -> Maybe Int
duplica Nothing = Nothing
duplica (Just x) = Just (x * 2)

duplica' :: Maybe Int -> Maybe Int
duplica' mx = do
  x <- mx

  --   Just $ 2 * x
  return $ 2 * x

geraAleatorio :: IO Int
geraAleatorio = do
  x <- randomRIO (1, 10)

  return x

getName :: IO String
getName = do
  putStr "Qual o teu nome? "
  name <- getLine

  return name

main = do
  name <- getName

  putStrLn $ "Hello, " ++ name