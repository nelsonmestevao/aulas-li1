module Main where

import Graphics.Gloss
import Graphics.Gloss.Data.Bitmap (loadBMP)
import Graphics.Gloss.Interface.IO.Game
import Snake
import System.Random

data Estado = Estado
  { jogoCobra :: JogoCobra,
    imagemMaca :: Picture
  }

type World = Estado

quadrado :: Float -> Picture
quadrado l = Polygon $ rectanglePath l l

desenha :: World -> IO Picture
desenha e@Estado {jogoCobra = jogo@JogoCobra {cobra = (Cobra direcao corpo), maca = (m : ms)}} = do
  return $ Pictures $ desenhaMaca m (imagemMaca e) : map desenhaCelula corpo

desenhaMaca :: Maca -> Picture -> Picture
desenhaMaca (x, y) imagem = Translate (x * 2 * raio) (y * 2 * raio) $ imagem
  where
    raio = 10

desenhaCelula :: Ponto -> Picture
desenhaCelula (x, y) = Color green $ Translate (x * tamanho) (y * tamanho) $ quadrado tamanho
  where
    tamanho = 20

reageEventos :: Event -> World -> IO World
reageEventos (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta Subir $ cobra j}}
reageEventos (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta Descer $ cobra j}}
reageEventos (EventKey (SpecialKey KeyLeft) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta VirarEsquerda $ cobra j}}
reageEventos (EventKey (SpecialKey KeyRight) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta VirarDireita $ cobra j}}
reageEventos _ w = return $ w

reageTempo :: Float -> World -> IO World
reageTempo _ e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = atualiza jogo}

window :: Display
window = InWindow "Jogo da Cobrinha" (1280, 720) (0, 0)

background :: Color
background = white

frameRate :: Int
frameRate = 5

estadoInicial :: Int -> Picture -> World
estadoInicial semente imagem =
  Estado
    { jogoCobra =
        JogoCobra
          { cobra = Cobra Cima [(0, 0), (0, -1), (0, -2), (0, -3)],
            maca = mymap $ geraNumerosAleatorios semente
          },
      imagemMaca = imagem
    }

mymap :: [Int] -> [(Float, Float)]
mymap [] = []
mymap [x] = []
mymap (x : y : t) = (fromIntegral x, fromIntegral y) : mymap t

geraNumerosAleatorios :: Int -> [Int]
geraNumerosAleatorios semente = map colocaNoIntervalo lista
  where
    gerador = mkStdGen semente
    lista = randoms gerador
    colocaNoIntervalo x = if mod x 2 == 0 then mymod x else (-1) * (mymod x)
    mymod x = mod x 20

main = do
  semente <- randomRIO (0, 2024)
  imagemMaca <- loadBMP "apple.bmp"
  playIO window background frameRate (estadoInicial semente imagemMaca) desenha reageEventos reageTempo
