module Snake where

type Ponto = (Float, Float)

type Maca = Ponto

data JogoCobra = JogoCobra
  { cobra :: Cobra,
    maca :: Maca
  }

data Direcao
  = Cima
  | Baixo
  | Esquerda
  | Direita
  deriving (Show, Eq, Ord)

data Cobra = Cobra {direcao :: Direcao, corpo :: [Ponto]} deriving (Show, Eq)

data Movimento = Subir | Descer | VirarEsquerda | VirarDireita deriving (Show, Eq, Ord)

movimenta :: Movimento -> Cobra -> Cobra
movimenta m c@(Cobra {direcao = direcao}) = c {direcao = novaDirecao}
  where
    novaDirecao = case m of
      Subir -> Cima
      Descer -> Baixo
      VirarEsquerda -> Esquerda
      VirarDireita -> Direita

-- novaDirecao | m == Subir = Cima
--             | m == Descer = Baixo
--             | m == VirarEsquerda = Esquerda
--             | m == VirarDireita = Direita

-- | Avança o corpo da cobra na direcao atual.
--
-- == Exemplos
--
-- >>> atualiza (Cobra Cima [(0, 0), (0, -1)])
-- Cobra Cima [(0, 1), (0, 0)]
atualiza :: Cobra -> Maca -> Cobra
atualiza c@(Cobra {direcao = direcao, corpo = (h : t)}) m = c {corpo = cabeca : h : cauda}
  where
    cabeca = novaCabeca direcao h
    cauda = if m == h then t else init t

novaCabeca :: Direcao -> Ponto -> Ponto
novaCabeca direcao (x, y) = case direcao of
  Cima -> (x, y + 1)
  Baixo -> (x, y - 1)
  Esquerda -> (x - 1, y)
  Direita -> (x + 1, y)
