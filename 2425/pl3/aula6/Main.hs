module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Snake

type World = JogoCobra

quadrado :: Float -> Picture
quadrado l = Polygon $ rectanglePath l l

desenha :: World -> Picture
desenha jogo@JogoCobra {cobra = (Cobra direcao corpo), maca = maca} =
  Pictures $ desenhaMaca maca : map desenhaCelula corpo

desenhaMaca :: Maca -> Picture
desenhaMaca (x, y) = Color red $ Translate (x * 2 * raio) (y * 2 * raio) $ circleSolid raio
  where
    raio = 10

desenhaCelula :: Ponto -> Picture
desenhaCelula (x, y) = Color green $ Translate (x * tamanho) (y * tamanho) $ quadrado tamanho
  where
    tamanho = 20

reageEventos :: Event -> World -> World
reageEventos (EventKey (SpecialKey KeyUp) Down _ _) j = j {cobra = movimenta Subir $ cobra j}
reageEventos (EventKey (SpecialKey KeyDown) Down _ _) j = j {cobra = movimenta Descer $ cobra j}
reageEventos (EventKey (SpecialKey KeyLeft) Down _ _) j = j {cobra = movimenta VirarEsquerda $ cobra j}
reageEventos (EventKey (SpecialKey KeyRight) Down _ _) j = j {cobra = movimenta VirarDireita $ cobra j}
reageEventos _ w = w

reageTempo :: Float -> World -> World
reageTempo _ jogo@JogoCobra {cobra = cobra, maca = maca} = jogo {cobra = atualiza cobra maca}

window :: Display
window = InWindow "Jogo da Cobrinha" (1280, 720) (0, 0)

background :: Color
background = white

frameRate :: Int
frameRate = 5

estadoInicial :: World
estadoInicial =
  JogoCobra
    { cobra = Cobra Cima [(0, 0), (0, -1), (0, -2), (0, -3)],
      maca = (20, 20)
    }

main = do
  play window background frameRate estadoInicial desenha reageEventos reageTempo
