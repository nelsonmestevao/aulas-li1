Como gerar a documentação!

1. Documentar o nosso código com notação do Haddock (comentários começam com |)
2. Gerar a documentação com o seguinte comando:
    $ haddock -h -o doc/html ficheiro.hs
= haddock: programa que gera documentação
= -h: para gerar html (geramos sempre html)
= -o <diretoria>: output, ou seja, onde vamos gerar a documentaçao,
     no exemplo, é na pasta doc/html
= ficheiro.hs: tem de ser o nome do nosso programa em Haskell
3. Linux com Firefox: 
    $ firefox doc/html/index.html
   MacOs:
    $ open doc/html/index.html
   Windows Subsystem for Linux:
    $ cd doc/html && python3 -m http.server 4242
    Visitar um browser no http://localhost:4242

# Notas

1. O ficheiro tem de ter um modulo (excepto Main)
2. esquercer-mos do |
3. faltar espacamento
4. Nao validar o html gerado