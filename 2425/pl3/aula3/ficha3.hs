module Ficha3 where

-- | O 'Nome' é uma string.
type Nome = String

-- | Coordenadas num referencial cartesiano 
type Coordenada = (Double, Double)

-- | Conjunto de opções de movimentos que um 'PosicaoPessoa' pode tomar.
data Movimento 
    = N -- ^ Norte
    | S -- ^ Sul
    | E -- ^ Este
    | W -- ^ Oeste
     deriving (Show,Eq)

-- | Simplesmente uma lista de 'Movimento'
type Movimentos = [Movimento]

-- | Definição em que identifica uma Pessoa por 'Nome' e também a sua posição atual.
data PosicaoPessoa = Pos Nome Coordenada deriving (Show,Eq)

-- a)
{-|
Dada uma lista de posições de pessoas, atualize essa lista depois de todas
executarem um movimento dado.

>>> posicoesM [] N
[]

>>> posicoesM [Pos "Nelson" (0, 0)] N
[Pos "Nelson" (0.0,1.0)]
-}
posicoesM :: [PosicaoPessoa] -> Movimento -> [PosicaoPessoa]
posicoesM [] _ = []
posicoesM (p:ps) m = (movimenta p m):posicoesM ps m
    where
        movimenta :: PosicaoPessoa -> Movimento -> PosicaoPessoa
        movimenta (Pos nome (x, y)) m = case m of 
                N -> Pos nome (x, y + 1)
                S -> Pos nome (x, y - 1)
                E -> Pos nome (x + 1, y)
                W -> Pos nome (x - 1, y)