-- f x = if mod x 2 == 0 then "Par" else "Impar"
f :: Int -> String
f x = if even x then "Par" else "Impar"

g :: Int -> Int
g x = x + x