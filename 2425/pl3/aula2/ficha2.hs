data Movimento = Norte | Sul | Este | Oeste deriving (Show, Eq)

type Ponto = (Double, Double)

-- (a)
move :: Ponto -> Movimento -> Ponto
-- move (x, y) Norte = (x, y + 1)
-- move (x, y) Sul = (x, y - 1)
-- move (x, y) Este = (x + 1, y)
-- move (x, y) Oeste = (x - 1, y)
move (x, y) movimento = case movimento of
  Norte -> (x, y + 1)
  Sul -> (x, y - 1)
  Este -> (x + 1, y)
  Oeste -> (x - 1, y)

-- move (x, y) movimento
--   | movimento == Norte = (x, y + 1)
--   | movimento == Sul = (x, y - 1)
--   | movimento == Este = (x + 1, y)
--   | movimento == Oeste = (x - 1, y)

-- (b)
dist :: Ponto -> Ponto -> Double
dist (a, b) (x, y) = sqrt ((a - x) ^ 2 + (b - y) ^ 2)

maisSul :: Ponto -> Ponto -> Ponto
maisSul (a, b) (x, y) = if b > y then (x, y) else (a, b)

-- maisSul p1@(a, b) p2@(x, y) = if b > y then p2 else p1

-- (7)
type Velocidade = (Double, Double)

type Tempo = Double

move' :: Ponto -> Velocidade -> Tempo -> Ponto
move' (x, y) (vx, vy) t = (x + dx, y + dy)
  where
    -- distancia percorrida na componente x
    dx = vx * t
    -- distancia percorrida na componente y
    dy = vy * t

-- O Double significa Raio (por isso tem de ser positivo)
data Figura = Circulo Ponto Double

pontoDentro :: Figura -> Ponto -> Bool
pontoDentro f p = 
pontoDentro (Circulo ponto raio) p = 
pontoDentro (Circulo (x, y) raio) p = 