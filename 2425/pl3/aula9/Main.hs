module Main where

import Data.Maybe
import Graphics.Gloss
import Graphics.Gloss.Data.Bitmap (loadBMP)
import Graphics.Gloss.Interface.IO.Game
import Snake
import System.Exit (exitSuccess)
import System.Random

data NomesImagens = MacaImagem deriving (Eq)

data OpcaoMenuInicial = Jogar | Sair

data Menu = MenuInicial OpcaoMenuInicial | ModoJogo

data Estado = Estado
  { jogoCobra :: JogoCobra,
    imagens :: [(NomesImagens, Picture)],
    menu :: Menu
  }

type World = Estado

quadrado :: Float -> Picture
quadrado l = Polygon $ rectanglePath l l

desenha :: World -> IO Picture
desenha e@Estado {menu = MenuInicial Jogar} = do
  return $ Pictures $ [Color blue $ Translate (-100) (70) $ Text "Jogar", Translate (-100) (-50) $ Text "Sair"]
desenha e@Estado {menu = MenuInicial Sair} = do
  return $ Pictures $ [Translate (-100) (70) $ Text "Jogar", Color blue $ Translate (-100) (-50) $ Text "Sair"]
desenha e@Estado {jogoCobra = jogo@JogoCobra {cobra = (Cobra direcao corpo), maca = (m : ms)}} = do
  return $ Pictures $ desenhaMaca m (getImagem MacaImagem $ imagens e) : map desenhaCelula corpo

getImagem :: NomesImagens -> [(NomesImagens, Picture)] -> Picture
getImagem nome imagens = fromJust $ lookup nome imagens

desenhaMaca :: Maca -> Picture -> Picture
desenhaMaca (x, y) imagem = Translate (x * 2 * raio) (y * 2 * raio) $ imagem
  where
    raio = 10

desenhaCelula :: Ponto -> Picture
desenhaCelula (x, y) = Color green $ Translate (x * tamanho) (y * tamanho) $ quadrado tamanho
  where
    tamanho = 20

reageEventos :: Event -> World -> IO World
reageEventos key e@Estado {menu = MenuInicial _} = reageEventosMenuInicial key e
reageEventos key e@Estado {menu = ModoJogo} = reageEventosModoJogo key e
reageEventos _ w = return $ w

reageEventosMenuInicial (EventKey (SpecialKey KeyEnter) Down _ _) e@Estado {menu = MenuInicial Jogar} = return $ e {menu = ModoJogo}
reageEventosMenuInicial (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {menu = MenuInicial Jogar} = return $ e {menu = MenuInicial Sair}
reageEventosMenuInicial (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {menu = MenuInicial Sair} = return $ e {menu = MenuInicial Jogar}
reageEventosMenuInicial (EventKey (SpecialKey KeyEnter) Down _ _) e@Estado {menu = MenuInicial Sair} = exitSuccess
reageEventosMenuInicial _ w = return $ w

reageEventosModoJogo (EventKey (Char 'p') Down _ _) e = return $ e {menu = MenuInicial Jogar}
reageEventosModoJogo (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta Subir $ cobra j}}
reageEventosModoJogo (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta Descer $ cobra j}}
reageEventosModoJogo (EventKey (SpecialKey KeyLeft) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta VirarEsquerda $ cobra j}}
reageEventosModoJogo (EventKey (SpecialKey KeyRight) Down _ _) e@Estado {jogoCobra = j} = return $ e {jogoCobra = j {cobra = movimenta VirarDireita $ cobra j}}
reageEventosModoJogo _ w = return $ w

reageTempo :: Float -> World -> IO World
reageTempo _ e@Estado {menu = MenuInicial _} = return $ e
reageTempo _ e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = atualiza jogo}

window :: Display
window = InWindow "Jogo da Cobrinha" (1280, 720) (0, 0)

background :: Color
background = white

frameRate :: Int
frameRate = 5

estadoInicial :: Int -> Picture -> World
estadoInicial semente imagem =
  Estado
    { jogoCobra =
        JogoCobra
          { cobra = Cobra Cima [(0, 0), (0, -1), (0, -2), (0, -3)],
            maca = mymap $ geraNumerosAleatorios semente
          },
      imagens = [(MacaImagem, imagem)],
      menu = MenuInicial Jogar
    }

mymap :: [Int] -> [(Float, Float)]
mymap [] = []
mymap [x] = []
mymap (x : y : t) = (fromIntegral x, fromIntegral y) : mymap t

geraNumerosAleatorios :: Int -> [Int]
geraNumerosAleatorios semente = map colocaNoIntervalo lista
  where
    gerador = mkStdGen semente
    lista = randoms gerador
    colocaNoIntervalo x = if mod x 2 == 0 then mymod x else (-1) * (mymod x)
    mymod x = mod x 20

main = do
  semente <- randomRIO (0, 2024)
  imagemMaca <- loadBMP "apple.bmp"
  playIO window background frameRate (estadoInicial semente imagemMaca) desenha reageEventos reageTempo
