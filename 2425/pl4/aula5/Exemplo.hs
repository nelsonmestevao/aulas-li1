-- \$ # para quem tem Ubuntu (WSL incluido)

-- \$ sudo apt update && sudo apt install freeglut3-dev

-- \$ cabal update && cabal install --lib gloss
-- \$ runhaskell Exemplo.hs
module Main where

import Graphics.Gloss

window :: Display
window = InWindow "Exemplo Aula 5" (1280, 720) (0, 0)

background :: Color
background = white

image :: Picture
image = Pictures [quadrado, circulo]
  where
    circulo = Scale (2) (2) $ Color red $ Circle 50
    quadrado = Rotate (45) $ Translate (-50) (-50) $ Color green $ Polygon [(0, 0), (0, 100), (100, 100), (100, 0)]

main = display window background image