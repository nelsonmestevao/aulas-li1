module Snake where

data Direcao = Cima | Baixo | Esquerda | Direita deriving (Show, Eq, Ord)

type Ponto = (Float, Float)

data Cobra = Cobra Direcao [Ponto] deriving (Show, Eq)

atualiza :: Cobra -> Cobra
atualiza (Cobra direcao pontos) = Cobra direcao novosPontos
  where
    -- novosPontos = map (\p -> atualizaPonto direcao p) pontos
    novosPontos = map (atualizaPonto direcao) pontos

atualizaPonto :: Direcao -> Ponto -> Ponto
atualizaPonto direcao (x, y) = case direcao of
  Cima -> (x, y + 1)
  Baixo -> (x, y - 1)
  Esquerda -> (x - 1, y)
  Direita -> (x + 1, y)
