module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Snake

type World = Cobra

desenha :: World -> Picture
desenha (Cobra direcao pontos) = Pictures $ map desenhaPonto pontos

desenhaPonto :: Ponto -> Picture
desenhaPonto (x, y) = Color green $ Translate (tamanho * x) (tamanho * y) $ quadrado tamanho
  where
    tamanho = 10

quadrado :: Float -> Picture
quadrado l = Polygon $ rectanglePath l l

reageEventos :: Event -> World -> World
reageEventos _ w = w

reageTempo :: Float -> World -> World
reageTempo _ w = atualiza w

window :: Display
window = InWindow "Jogo da Cobrinha" (1280, 720) (500, 500)

background :: Color
background = greyN 0.6

frameRate :: Int
frameRate = 10

main =
  play window background frameRate jogoInicio desenha reageEventos reageTempo
  where
    jogoInicio = Cobra Cima [(0, -10), (0, -11)]