module Monads where

import System.Random

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x : xs) = return x

geraAleatorio :: IO Int
geraAleatorio = do
  x <- randomRIO (1, 9)
  y <- randomRIO (1, 9)

  putStrLn $ "x = " ++ show x
  putStrLn $ "y = " ++ show y

  return $ x + y

exemplo = do
  let numeros = [42, 5, 8]

  x <- safeHead numeros
  y <- safeHead []

  return $ x + 1

getName :: IO String
getName = do
  putStr "What is your name? "
  name <- getLine

  return name

main = do
  k <- geraAleatorio
  print k
  name <- getName
  putStrLn $ "Hello, " ++ name
