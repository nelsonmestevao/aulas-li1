module Ficha1 where

duplica :: Int -> Int
duplica x = x + x

soma :: Int -> Int -> Int
soma x y = x + y

somaL :: [Int] -> Int
somaL lista = sum lista

hello :: String -> String
hello name = "Hello, " ++ name

-- Defina uma função que recebe uma lista não vazia e devolve um par com o primeiro
-- e último elemento da lista.

primult :: [a] -> (a, a)
primult lista = (primeiro, ultimo)
  where
    primeiro = head lista
    ultimo = last lista

tultimo :: [String] -> Int
tultimo [] = 0
tultimo nomes = length apelido
  where
    apelido = last nomes

d :: [String] -> Int
-- d nomes
--   | null nomes = 0
--   | otherwise = length (last nomes)
d [] = 0
d nomes = length (last nomes)

k :: [String] -> String
-- k nomes = head (head nomes) : "." ++ last nomes
k nomes = inicial_primeiro_nome : '.' : last nomes
  where
    inicial_primeiro_nome = dhead nomes

dhead :: [[a]] -> a
dhead ns = head (head ns)

-- k (primeiro : nomes) = (head primeiro) : '.' : last nomes
-- k ((p : ps) : nomes) = p : '.' : last nomes
