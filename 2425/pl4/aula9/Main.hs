module Main where

import Data.Maybe
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Snake
import System.Exit (exitSuccess)
import System.Random

data OpcaoMenuInicial = Jogar | Sair deriving (Eq)

data Menu = MenuInicial OpcaoMenuInicial | ModoJogo deriving (Eq)

data NomeImagem = ImagemMaca | ImagemCabecaCobra deriving (Eq)

data Estado = Estado
  { jogoCobra :: JogoCobra,
    imagens :: [(NomeImagem, Picture)],
    menu :: Menu
  }

type World = Estado

desenha :: World -> IO Picture
desenha e@Estado {menu = MenuInicial Jogar} = do
  return $
    Pictures $
      [ Color red $ Translate (-150) 0 $ Text "Jogar",
        Translate (-150) (-100) $ Text "Sair"
      ]
desenha e@Estado {menu = MenuInicial Sair} = do
  return $
    Pictures $
      [ Translate (-150) 0 $ Text "Jogar",
        Color red $ Translate (-150) (-100) $ Text "Sair"
      ]
desenha e@Estado {jogoCobra = j@JogoCobra {cobra = Cobra direcao pontos, maca = (maca : _)}} = do
  return $ Pictures $ desenhaMaca maca (fromJust $ lookup ImagemMaca $ imagens e) : map desenhaPonto pontos

desenhaMaca :: Maca -> Picture -> Picture
desenhaMaca (x, y) imagem = Translate (raio * 2 * x) (raio * 2 * y) $ Scale (0.8) (0.8) $ imagem
  where
    raio = 10

desenhaPonto :: Ponto -> Picture
desenhaPonto (x, y) = Color green $ Translate (tamanho * x) (tamanho * y) $ quadrado tamanho
  where
    tamanho = 20

quadrado :: Float -> Picture
quadrado l = Polygon $ rectanglePath l l

reageEventos :: Event -> World -> IO World
reageEventos key e@Estado {menu = MenuInicial _} = reageEventosMenuInicial key e
reageEventos key e@Estado {menu = ModoJogo} = reageEventosModoJogo key e
reageEventos _ w = return $ w

reageEventosMenuInicial (EventKey (SpecialKey KeyEnter) Down _ _) e@Estado {menu = MenuInicial Jogar} = return $ e {menu = ModoJogo}
reageEventosMenuInicial (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {menu = MenuInicial Jogar} = return $ e {menu = MenuInicial Sair}
reageEventosMenuInicial (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {menu = MenuInicial Sair} = return $ e {menu = MenuInicial Jogar}
reageEventosMenuInicial (EventKey (SpecialKey KeyEnter) Down _ _) e@Estado {menu = MenuInicial Sair} = exitSuccess
reageEventosMenuInicial _ e = return $ e

reageEventosModoJogo (EventKey (Char 'p') Down _ _) e = return $ e {menu = MenuInicial Jogar}
reageEventosModoJogo (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta Subir jogo}
reageEventosModoJogo (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta Descer jogo}
reageEventosModoJogo (EventKey (SpecialKey KeyLeft) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta VirarEsquerda jogo}
reageEventosModoJogo (EventKey (SpecialKey KeyRight) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta VirarDireita jogo}
reageEventosModoJogo _ e = return e

reageTempo :: Float -> World -> IO World
reageTempo _ e@Estado {menu = MenuInicial _} = return $ e
reageTempo _ e@Estado {menu = ModoJogo, jogoCobra = jogo} = return $ e {jogoCobra = atualiza jogo}

window :: Display
window = InWindow "Jogo da Cobrinha" (1280, 720) (500, 500)

background :: Color
background = white

frameRate :: Int
frameRate = 10

transformaPares :: [Int] -> [(Float, Float)]
transformaPares [] = []
transformaPares [x] = []
transformaPares (x : y : t) = (fromIntegral x, fromIntegral y) : transformaPares t

loadImagens :: IO [(NomeImagem, Picture)]
loadImagens = do
  imgMaca <- loadBMP "apple.bmp"
  return [(ImagemMaca, imgMaca)]

main :: IO ()
main = do
  semente <- randomRIO (0, 2024)
  imgs <- loadImagens
  playIO window background frameRate (jogoInicio semente imgs) desenha reageEventos reageTempo
  where
    macas semente = transformaPares $ map normalizaCoordenadas $ randoms $ mkStdGen semente
    normalizaCoordenadas x = if x > 0 then mod x 20 else (-1) * mod x 20
    jogoInicio semente imagens = Estado {menu = MenuInicial Jogar, jogoCobra = JogoCobra {cobra = Cobra Cima [(0, -10), (0, -11), (0, -12), (0, -13)], maca = macas semente}, imagens = imagens}