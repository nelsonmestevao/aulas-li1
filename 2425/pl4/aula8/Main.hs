module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Snake
import System.Random

data Estado = Estado
  { jogoCobra :: JogoCobra,
    imagemMaca :: Picture
  }

type World = Estado

desenha :: World -> IO Picture
desenha e@Estado {jogoCobra = j@JogoCobra {cobra = Cobra direcao pontos, maca = (maca : _)}} = do
  return $ Pictures $ desenhaMaca maca (imagemMaca e) : map desenhaPonto pontos

desenhaMaca :: Maca -> Picture -> Picture
desenhaMaca (x, y) imagem = Translate (raio * 2 * x) (raio * 2 * y) $ Scale (0.8) (0.8) $ imagem
  where
    raio = 10

desenhaPonto :: Ponto -> Picture
desenhaPonto (x, y) = Color green $ Translate (tamanho * x) (tamanho * y) $ quadrado tamanho
  where
    tamanho = 20

quadrado :: Float -> Picture
quadrado l = Polygon $ rectanglePath l l

reageEventos :: Event -> World -> IO World
reageEventos (EventKey (SpecialKey KeyUp) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta Subir jogo}
reageEventos (EventKey (SpecialKey KeyDown) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta Descer jogo}
reageEventos (EventKey (SpecialKey KeyLeft) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta VirarEsquerda jogo}
reageEventos (EventKey (SpecialKey KeyRight) Down _ _) e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = movimenta VirarDireita jogo}
reageEventos _ w = return $ w

reageTempo :: Float -> World -> IO World
reageTempo _ e@Estado {jogoCobra = jogo} = return $ e {jogoCobra = atualiza jogo}

window :: Display
window = InWindow "Jogo da Cobrinha" (1280, 720) (500, 500)

background :: Color
background = white

frameRate :: Int
frameRate = 10

transformaPares :: [Int] -> [(Float, Float)]
transformaPares [] = []
transformaPares [x] = []
transformaPares (x : y : t) = (fromIntegral x, fromIntegral y) : transformaPares t

main :: IO ()
main = do
  semente <- randomRIO (0, 2024)
  img <- loadBMP "apple.bmp"
  playIO window background frameRate (jogoInicio semente img) desenha reageEventos reageTempo
  where
    macas semente = transformaPares $ map normalizaCoordenadas $ randoms $ mkStdGen semente
    normalizaCoordenadas x = if x > 0 then mod x 20 else (-1) * mod x 20
    jogoInicio semente imagem = Estado {jogoCobra = JogoCobra {cobra = Cobra Cima [(0, -10), (0, -11), (0, -12), (0, -13)], maca = macas semente}, imagemMaca = imagem}