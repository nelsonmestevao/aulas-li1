data Movimento = Norte | Sul | Este | Oeste deriving (Show, Eq)

type Ponto = (Double, Double)

-- (a)
move :: Ponto -> Movimento -> Ponto
-- move p Norte = (fst p, snd p + 1)
-- move p Sul = (fst p, snd p - 1)
-- move p Este = (fst p + 1, snd p)
-- move p Oeste = (fst p - 1, snd p)
-- move (x, y) Norte = (x, y + 1)
-- move (x, y) Sul = (x, y - 1)
-- move (x, y) Este = (x + 1, y)
-- move (x, y) Oeste = (x - 1, y)
move (x, y) m = case m of
  Norte -> (x, y + 1)
  Sul -> (x, y - 1)
  Este -> (x + 1, y)
  Oeste -> (x - 1, y)

-- move (x, y) m
--   | m == Norte = (x, y + 1)
--   | m == Sul = (x, y - 1)
--   | m == Este = (x + 1, y)
--   | m == Oeste = (x - 1, y)

dist :: Ponto -> Ponto -> Double
dist (x1, y1) (x2, y2) = sqrt ((x2 - x1) ^ 2 + (y2 - y1) ^ 2)

sul :: Ponto -> Ponto -> Ponto
sul p1@(x1, y1) p2@(x2, y2) = if y1 >= y2 then p2 else p1

-- 2

-- move2 :: Ponto -> Movimento -> Double -> Ponto
-- move2 (x, y) m l = case m of
--   Norte -> (x, sumcap y 1 l)
--   Sul -> (x, sumcap y (-1) l)
--   Este -> (sumcap x 1 l, y)
--   Oeste -> (sumcap x (-1) l, y)
--   where
--     sumcap = somalimitada

-- somalimitada coordenada valor limite =
--   if coordenada + valor > limite
--     then coordenada
--     else coordenada + valor

move2 :: Ponto -> Movimento -> Double -> Ponto
move2 (x, y) m l = case m of
  Norte -> (x, min (y + 1) l)
  Sul -> (x, max (y - 1) 0)
  Este -> (min (x + 1) l, y)
  Oeste -> (max (x - 1) 0, y)

-- 7
type Velocidade = (Double, Double)

type Tempo = Double

move7 :: Ponto -> Velocidade -> Tempo -> Ponto
move7 (x, y) (vx, vy) t = (x + vx * t, y + vy * t)