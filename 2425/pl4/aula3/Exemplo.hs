{-|
Module : Exemplo
Description : Módulo Haskell contendo funções recursivas.
Copyright : (c) Nelson <d12733@di.uminho.pt>, 2024
Maintainer : nelson@estevao.org

Este módulo contém definições Haskell para o cálculo de funções recursivas
simples (obs: isto é apenas uma descrição.
-}
module Exemplo where

-- | Inteiro para representar naturais.
type Natural = Int

{-|
Função fatorial implentada em Haskell /recursivamente/.

= Notas

* Esta função __não funciona__ para inteiros negativos.
* Esta função recebe 'Natural' que não deixa de ser um 'Int'.

= Exemplos

>>> fact 0
1

>>> fact 5
120
-}
fact :: Int -> Int
fact 0 = 1
fact n = n * fact (n - 1)
