{-|
Module : Ficha3
Description : Resolução da Ficha 3 de LI1
Copyright : (c) Nelson <d12733@di.uminho.pt>, 2024

Exercícios resolvidos da Ficha 3 na Aula 3 de Laboratórios de Informática I para
o ano letivo 2024/2025.
-}
module Ficha3 where

type Nome = String
type Coordenada = (Double, Double)

-- | Possibilidade de Movimentos que um 'PosicaoPessoa' pode tomar.
data Movimento
    = N -- ^ Norte
    | S -- ^ Sul
    | E -- ^ Este
    | W -- ^ Oeste
    deriving (Show,Eq)

type Movimentos = [Movimento]

data PosicaoPessoa = Pos Nome Coordenada deriving (Show,Eq)

{-| Movimenta uma lista de pessoas de igual forma.

== Exemplos

>>> posicoesM [Pos "Nelson" (0, 0)] N
[Pos "Nelson" (0.0,1.0)]
-}
posicoesM
    :: [PosicaoPessoa] -- ^ Lista de 'PosicaoPessoa'
    -> Movimento  -- ^ Movimento a aplicar a todas as 'PosicaoPessoa'
    -> [PosicaoPessoa] -- ^ Lista com as coordenadas atualizadas.
posicoesM [] m = []
posicoesM (p:ps) m = movimenta p m : posicoesM ps m
    where
        movimenta :: PosicaoPessoa -> Movimento -> PosicaoPessoa
        movimenta (Pos nome c) m = Pos nome (movimentaC c m)
        movimentaC :: Coordenada -> Movimento -> Coordenada
        movimentaC (x, y) m = case m of
            N -> ((x, y + 1))
            S -> ((x, y - 1))
            E -> ((x + 1, y))
            W -> ((x - 1, y))

posicao :: PosicaoPessoa -> Movimentos -> PosicaoPessoa
posicao p [] = p
posicao p (m:ms) = posicao (movimenta p m) ms
        where
            movimenta :: PosicaoPessoa -> Movimento -> PosicaoPessoa
            movimenta (Pos nome c) m = Pos nome (movimentaC c m)
            movimentaC :: Coordenada -> Movimento -> Coordenada
            movimentaC (x, y) m = case m of
                N -> ((x, y + 1))
                S -> ((x, y - 1))
                E -> ((x + 1, y))
                W -> ((x - 1, y))

-- posicao :: PosicaoPessoa -> Movimentos -> PosicaoPessoa
-- posicao p [] = p
-- posicao p (m:ms) = let novaPessoa = movimenta p m
--         in posicao (novaPessoa) ms
--         where
--             movimenta :: PosicaoPessoa -> Movimento -> PosicaoPessoa
--             movimenta (Pos nome c) m = Pos nome (movimentaC c m)
--             movimentaC :: Coordenada -> Movimento -> Coordenada
--             movimentaC (x, y) m = case m of
--                 N -> ((x, y + 1))
--                 S -> ((x, y - 1))
--                 E -> ((x + 1, y))
--                 W -> ((x - 1, y))