module Ficha2 where

type Nome = String

type Coordenada = (Double, Double)

data Movimento = N | S | E | W deriving (Show, Eq) -- norte, sul, este, oeste

type Movimentos = [Movimento]

data PosicaoPessoa = Pos Nome Coordenada deriving (Show, Eq)

a = Pos "Nelson" (0, 5)

b = Pos "Rodrigo" (2, 10)

c = Pos "Tome" (-2, -15)

lista = [a, b, c]

-- |
-- Retorna PosicaoPessoa que tem a coordenada y maior (ou seja, pessoas que está
-- mais a Norte).
--
-- == Exemplos
--
-- >>> pessoaMaisNorte [Pos "Nelson" (0, 5), Pos "Rodrigo" (2, 10), Pos "Tome" (-2, -15)]
-- Just (Pos "Rodrigo" (2.0,10.0))
--
-- >>> pessoaMaisNorte []
-- Nothing
--
-- >>> pessoaMaisNorte [Pos "Nelson" (0, 5)]
-- Just (Pos "Nelson" (0.0,5.0))
pessoaMaisNorte :: [PosicaoPessoa] -> Maybe PosicaoPessoa
pessoaMaisNorte [] = Nothing
pessoaMaisNorte (p : ps) = Just $ getMaisNorte p pmn
  where
    pmn = pessoaMaisNorte ps

getMaisNorte :: PosicaoPessoa -> Maybe PosicaoPessoa -> PosicaoPessoa
getMaisNorte p1 Nothing = p1
getMaisNorte p1@(Pos nome1 (x1, y1)) (Just p2@(Pos nome2 (x2, y2))) =
  if y1 >= y2 then p1 else p2

-- |
-- Retorna PosicaoPessoa que tem a coordenada y maior (ou seja, pessoas que está
-- mais a Norte).
--
-- == Exemplos
--
-- >>> pessoaNorte [Pos "Nelson" (0, 5), Pos "Rodrigo" (2, 10), Pos "Tome" (-2, -15)]
-- Just (Pos "Rodrigo" (2.0,10.0))
--
-- >>> pessoaNorte []
-- Nothing
--
-- >>> pessoaNorte [Pos "Nelson" (0, 5)]
-- Just (Pos "Nelson" (0.0,5.0))
pessoaNorte :: [PosicaoPessoa] -> Maybe PosicaoPessoa
pessoaNorte [] = Nothing
pessoaNorte [x] = Just x
pessoaNorte (p1@(Pos _ (_, y1)) : p2@(Pos _ (_, y2)) : t) =
  if y1 > y2
    then pessoaNorte (p1 : t)
    else pessoaNorte (p2 : t)

-- 5)

-- | Desloca cada elemento de uma lista, @n@ posições para a direita.
--
-- == Exemplos
--
-- >>> desloca [1,2,3,4,5] 2
-- [4,5,1,2,3]
desloca :: [a] -> Int -> [a]
desloca l 0 = l
desloca l n = desloca (last l : init l) (n - 1)

mydrop :: [a] -> Int -> [a]
mydrop [] _ = []
mydrop l 0 = l
mydrop (x : xs) n = mydrop xs (n - 1)

mytake :: [a] -> Int -> [a]
mytake lista 0 = []
mytake (x : xs) n = x : mytake xs (n - 1)

-- | Desloca cada elemento de uma lista, @n@ posições para a direita.
--
-- == Exemplos
--
-- >>> desloca2 [1,2,3,4,5] 2
-- [4,5,1,2,3]
desloca2 :: [a] -> Int -> [a]
desloca2 l n = b ++ a
  where
    a = mytake l (cmp - n)
    b = mydrop l (cmp - n)
    cmp = length l
