module Questao1 where

-- estev.ao/li1-2425

import Test.HUnit

-- |
-- Retorna o indice em que um elemento se encontra. Se não, retorna Nothing.
--
-- == Exemplos
--
-- >>> f [] 2
-- Nothing
--
-- >>> f [1,2,3] 2
-- Just 1
f :: (Eq a) => [a] -> a -> Maybe Int
f l x = procura x li
  where
    li = zip l [0 ..]
    procura x [] = Nothing
    procura x ((y, n) : ys)
      | x == y = Just n
      | otherwise = procura x ys

teste1 = "quando o elemento nao existe" ~: Nothing ~=? f [1, 2, 3] 4

teste2 = "quando o unico elemento que existe é o que procuramos" ~: Just 0 ~=? f [2] 2

teste3 = "quando o unico elemento que existe nao é o que procuramos" ~: Nothing ~=? f [2] 5

testesF = test [teste1, teste2, teste3]

-- | Calcula uma lista de pares com o nome de cada pessoa e respectiva posição
-- na fila final, começando em 1.
--
-- == Exemplos
-- >>> g [["AA","BB","CC"],["DD","EE","FF","GG"]]
-- [("AA",1),("BB",2),("CC",3),("DD",4),("EE",5),("FF",6),("GG",7)]
--
-- >>> g [["AA","BB","CC"],[]]
-- [("AA",1),("BB",2),("CC",3)]
g :: [[String]] -> [(String, Int)]
g m = zip (concat m) [1 ..]

testesG =
  test
    [ "para matriz vazia" ~: [] ~=? g [],
      "para matriz com so uma linha" ~: [("Nelson", 1), ("Rodrigo", 2)] ~=? g [["Nelson", "Rodrigo"]],
      "para matriz com duas linhas" ~: [("Nelson", 1), ("Rodrigo", 2)] ~=? g [["Nelson"], ["Rodrigo"]]
    ]

main = runTestTTAndExit $ test [testesF, testesG]