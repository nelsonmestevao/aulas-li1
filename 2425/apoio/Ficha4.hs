-- |
-- Module      : Ficha4
-- Description : Testes Unitários em Haskell
-- Copyright   : Nelson Estevão <d12733@di.uminho.pt>
--
-- Testes Unitários em Haskell usando HUnit.
module Ficha4 where

type Matriz a = [[a]]

m :: Matriz Integer
m =
  [ [1, 2, 3, 4, 5, 6, 7, 8, 9],
    [10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
    [20, 21, 22, 23, 24, 25, 26, 27, 28, 29],
    [30, 31, 32, 33, 34, 35, 36, 37, 38, 39]
  ]

-- |
-- Troca a primeira linha de uma matriz __não vazia__ com a ultima linha.
--
-- == Exemplos
--
-- >>> trocaMatriz [[1,2], [3, 4]]
-- [[3,4],[1,2]]
--
-- >>> trocaMatriz [[1], [2], [3], [4]]
-- [[4],[2],[3],[1]]
trocaMatriz :: Matriz a -> Matriz a
trocaMatriz matriz = trocaLinha matriz

trocaLinha :: [a] -> [a]
trocaLinha [x] = [x]
trocaLinha lista = ultimo : meio ++ [primeiro]
  where
    primeiro = head lista
    meio = init (tail lista)
    ultimo = last lista

-- |
-- Troca a primeira coluna com a ultima de cada linha de uma matriz.
--
-- == Exemplos
--
-- >>> trocaColunas ["Nelson"]
-- ["nelsoN"]
--
-- >>> trocaColunas [[1,2,3,4], [5,6,7,8]]
-- [[4,2,3,1],[8,6,7,5]]
trocaColunas :: Matriz a -> Matriz a
trocaColunas [] = []
trocaColunas (l : ls) = trocaLinha l : trocaColunas ls

type Posicao = (Int, Int)

-- 5
procuraMatriz :: (Eq a) => a -> Matriz a -> Maybe Posicao
procuraMatriz = procuraMatrizAcc 0

procuraMatrizAcc :: (Eq a) => Int -> a -> Matriz a -> Maybe Posicao
procuraMatrizAcc i a [] = Nothing
procuraMatrizAcc i a (l : ls) = case procuraNaLinha a l of
  Nothing -> procuraMatrizAcc (i + 1) a ls
  Just j -> Just (j, i)

procuraNaLinha :: (Eq a) => a -> [a] -> Maybe Int
procuraNaLinha a [] = Nothing
procuraNaLinha a l = procuraLinhaAcc 0 a l

procuraLinhaAcc :: (Eq a) => Int -> a -> [a] -> Maybe Int
procuraLinhaAcc _ _ [] = Nothing
procuraLinhaAcc j a (x : xs) =
  if a == x
    then Just j
    else procuraLinhaAcc (j + 1) a xs

trocaElemNaMatriz :: Posicao -> a -> Matriz a -> Matriz a
trocaElemNaMatriz (i, 0) a (l : ls) = trocaElemNaLinha i a l : ls
trocaElemNaMatriz (i, j) a (l : ls) = l : trocaElemNaMatriz (i, j - 1) a ls

trocaElemNaLinha 0 a (x : xs) = (a : xs)
trocaElemNaLinha i a (x : xs) = x : trocaElemNaLinha (i - 1) a xs