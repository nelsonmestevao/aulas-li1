module Records where

data Aluno1 = Aluno1
  { nome :: String,
    nota :: Int
  }

data Aluno2 = Aluno2 String Int

f :: Aluno2 -> Int
f (Aluno2 nome nota) = nota

f :: Aluno1 -> Int
f (a@Auluno1 {nota = minhaNota}) = minhaNota
f a = nota a