module Ficha4Spec where

-- $ cabal update && cabal install --lib HUnit
import Test.HUnit

import Ficha4

trocaMatrizTeste1 = "para matriz com 1 linha so" ~: [[1]] ~=? trocaMatriz [[1]]

trocaMatrizTeste2 =
  "para matriz com 2 linhas" ~: [[2], [1]] ~=? trocaMatriz [[1], [2]]

trocaMatrizTeste3 =
  "para matriz com 3 linhas" ~: [[3], [2], [1]] ~=? trocaMatriz [[1], [2], [3]]

trocaMatrizTeste4 =
  "para matriz com mais de 3 linhas"
    ~: [[4], [2], [3], [1]]
    ~=? trocaMatriz [[1], [2], [3], [4]]

trocaMatrizTestes =
  test
    [trocaMatrizTeste1, trocaMatrizTeste2, trocaMatrizTeste3, trocaMatrizTeste4]

matrizVazia :: Matriz Int
matrizVazia = []

trocaColunasTestes =
  test
    [ "para a matriz vazia" ~: matrizVazia ~=? trocaColunas matrizVazia
    , "para a matriz com 1 linha" ~: ["nelsoN"] ~=? trocaColunas ["Nelson"]
    , "para a matriz mais de 3 linhas"
        ~: [[3, 2, 1], [6, 5, 4], [9, 8, 7]]
        ~=? trocaColunas [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    ]

todosOsTestes = test [trocaMatrizTestes, trocaColunasTestes]

-- $ runhaskell Ficha4Spec.hs
main = runTestTTAndExit todosOsTestes
