module Main where

type World = undefined

import Graphics.Gloss (Display (InWindow))

desenha :: World -> Picture
desenha w = undefined

reageEventos :: Event -> World -> World
reageEventos (EventKey (SpecialKey KeyUp) Down _ _) w = w
reageEventos e w = w

reageTempo :: Float -> World -> World
reageTempo t w = w

window :: Display
window = InWindow "Exemplo" (1080, 720) (0, 0)

background :: Color
background = white

frameRate :: Int
frameRate = 25


main = do
        play window background frameRate estadoInicial desenha reageEventos reageTempo
    where
        estadoInicial :: World
        estadoInicial = undefined