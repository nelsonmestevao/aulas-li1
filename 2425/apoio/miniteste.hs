-- 2.b)
type Horas = (Integer, Integer)

-- # Instalar o doctest:
-- > cabal update && cabal install doctest
-- > # Executar os exemplos da documentação
-- > doctest minitest.hs
{-|
Recebe a hora de início de uma viagem e a sua duração em minutos, e calcula a
hora de chegada.

== Considerações

Pode haver mudança de dia (por exemplo, se a viagem se iniciar às
(23, 55) e durar 25 minutos, terminará às (0, 20)). Considere ainda que a duração
da viagem é inferior a 24h.

== Exemplos

>>> f (23, 55) 30
(0,25)

>>> f (1, 0) 130
(3,10)
-}
f :: Horas -> Integer -> Horas
f (h, m) i = fromMinutos totalMinutos
  where
    totalMinutos = i + toMinutos (h, m)

toMinutos :: Horas -> Integer
toMinutos (h, m) = h * 60 + m

fromMinutos :: Integer -> Horas
fromMinutos i = (mod h 24, mod i 60)
  where
    h = div i 60
