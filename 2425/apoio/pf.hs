-- 8
myzip :: [a] -> [b] -> [(a, b)]
-- myzip [] [] = []
myzip [] _ = []
myzip _ [] = []
-- myzip (x:xs) (y:ys) = [(x,y)] ++ myzip xs ys
myzip (x:xs) (y:ys) = (x, y) : myzip xs ys

-- 17
fun :: [(a, b, c)] -> [(a, c)]
-- fun (x:xs) = 
fun [] = []
fun ((a, b, c):xs) = (a, c) : fun xs

-- 27
delete :: Eq a => a -> [a] -> [a]
delete _ [] = []
delete x (y:ys)
  | x /= y = y : delete x ys
  | otherwise = ys

-- 25
elemIndices :: Eq a => a -> [a] -> [Int]
-- elemIndices x (y:ys) = elemIndicesAc 0 x (y : ys)
elemIndices = elemIndicesAc 0

elemIndicesAc :: Eq a => Int -> a -> [a] -> [Int]
elemIndicesAc _ _ [] = []
elemIndicesAc i x (y:ys) =
  if x /= y
    then elemIndicesAc (i + 1) x ys
    else i : elemIndicesAc (i + 1) x ys

-- exemplo de um "reduce" -> "fold"
mylength :: [a] -> Int
mylength (a:as) = lengthAc 0 (a : as)

-- length [] = 0
-- length (a:as) = 1 + length as
lengthAc :: Int -> [a] -> Int
lengthAc n [] = n
lengthAc n (a:as) = lengthAc (n + 1) as

-- 2.a
myEnumFromThenTo :: Int -> Int -> Int -> [Int]
myEnumFromThenTo start next end =
  if start >= end
    then []
    else start : myEnumFromThenTo (start + intervalo) (next + intervalo) end
  where
    intervalo = next - start

-- 2.b
myEnumFromThenToIntervalo :: Int -> Int -> Int -> [Int]
myEnumFromThenToIntervalo start next end =
  myEnumFromThenToIntervaloAc start (next - start) end

myEnumFromThenToIntervaloAc :: Int -> Int -> Int -> [Int]
myEnumFromThenToIntervaloAc start intervalo end =
  if start >= end
    then []
    else start : myEnumFromThenToIntervaloAc (start + intervalo) intervalo end

-- 28
(\\\\) :: Eq a => [a] -> [a] -> [a]
-- (\\\\) [26, 12, 3, 1, 12] [3, 12]
(\\\\) [] _ = []
(\\\\) _ [] = []
(\\\\) (a:as) bs =
  if a `elem` bs
    then (\\\\) as (delete a bs)
    else a : (\\\\) as bs
