module Snake where

type Ponto = (Float, Float)

data Direcao
  = Cima
  | Baixo
  | Esquerda
  | Direita
  deriving (Show, Eq, Ord)

data Movimento = Sobe | Desce | ViraEsquerda | ViraDireita deriving (Show, Eq, Ord)

data Cobra = Cobra Direcao [Ponto] deriving (Show, Eq)

movimenta :: Movimento -> Cobra -> Cobra
movimenta m (Cobra direcao pontos) = case m of
  Sobe -> Cobra (Cima) pontos
  Desce -> Cobra (Baixo) pontos
  ViraEsquerda -> Cobra (Esquerda) pontos
  ViraDireita -> Cobra (Direita) pontos

atualiza :: Float -> Cobra -> Cobra
atualiza tempo (Cobra direcao (h : t)) = Cobra direcao (proximaCabeca h : h : init t)
  where
    proximaCabeca (x, y) = case direcao of
      Cima -> (x, y + 1)
      Baixo -> (x, y - 1)
      Esquerda -> (x - 1, y)
      Direita -> (x + 1, y)

--   where
-- novoPontos = map (\p -> atualizaPonto t direcao p) pontos
--     novoPontos = map (atualizaP) pontos
--     atualizaP :: Ponto -> Ponto
--     atualizaP p = atualizaPonto t direcao p

-- atualizaPonto :: Float -> Direcao -> Ponto -> Ponto
-- atualizaPonto t direcao (x, y) = case direcao of
--   Cima -> (x, y + 1 * t)
--   Baixo -> (x, y - 1 * t)
--   Esquerda -> (x - 1 * t, y)
--   Direita -> (x + 1 * t, y)