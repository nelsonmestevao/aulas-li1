module Main where

import Graphics.Gloss

window :: Display
window = InWindow "Exemplo Aula 5" (1280, 720) (0, 0)

background :: Color
background = white

images :: Picture
images = Pictures [quadrado, circulo]

circulo = Color blue $ Circle 50

quadrado = Translate (-50) (-50) $ Color rose $ Polygon [(1, 0), (0, 100), (100, 100), (100, 0)]

main = display window background images