module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Snake

type World = Cobra

mundo :: World
mundo = Cobra Cima [(0, 0), (0, -1), (0, -2), (0, -3)]

desenha :: World -> Picture
desenha (Cobra direcao pontos) = Pictures $ map desenhaPonto pontos

desenhaPonto :: Ponto -> Picture
desenhaPonto (x, y) = Translate (x * tamanho * 2) (y * tamanho * 2) $ Color green $ circleSolid tamanho
  where
    tamanho = 20

reageEventos :: Event -> World -> World
reageEventos (EventKey (SpecialKey KeyUp) Down _ _) cobra = movimenta Sobe cobra
reageEventos (EventKey (SpecialKey KeyDown) Down _ _) cobra = movimenta Desce cobra
reageEventos (EventKey (SpecialKey KeyLeft) Down _ _) cobra = movimenta ViraEsquerda cobra
reageEventos (EventKey (SpecialKey KeyRight) Down _ _) cobra = movimenta ViraDireita cobra
reageEventos e w = w

reageTempo :: Float -> World -> World
reageTempo t cobra = atualiza t cobra

window :: Display
window = InWindow "" (1280, 720) (0, 0)

background :: Color
background = white

frameRate :: Int
frameRate = 5

main = play window background frameRate mundo desenha reageEventos reageTempo