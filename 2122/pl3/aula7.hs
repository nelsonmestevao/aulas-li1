module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

data Jogo = Jogo (Int, Int) [(Int, Int)]

data Opcoes = Jogar
            | Sair

data Menu = Controlador Opcoes
          | ModoJogo Jogo
          | VenceuJogo

type World = (Menu, Jogo)

window :: Display
window = InWindow "Pacman" (720, 400) (0, 0)

fr :: Int
fr = 25

draw :: World -> Picture
draw (VenceuJogo, jogo) = Translate (-200) 0 $ Color red $ Text "Ganhou"
draw (Controlador Jogar, jogo) = Pictures [Color blue $ drawOption "Jogar", Translate (0) (-70) $ drawOption "Sair"]
draw (Controlador Sair, jogo) = Pictures [drawOption "Jogar", Color blue $ Translate (0) (-70) $ drawOption "Sair"]
draw (ModoJogo (Jogo (x, y) l), jogo) = Pictures $ (map drawSmallCircles l) ++ [Translate i j $ Color green (circleSolid 20)]
  where
      i = fromIntegral x
      j = fromIntegral y

drawOption option = Translate (-50) 0 $ Scale (0.5) (0.5) $ Text option

drawSmallCircles :: (Int, Int) -> Picture
drawSmallCircles (x, y) = Translate i j $ Circle 5
    where
      i = fromIntegral x
      j = fromIntegral y

engine :: (Int, Int) -> [(Int, Int)] -> Jogo
engine p l = Jogo p (filter (p/=) l)

event :: Event -> World -> World
event (EventKey (SpecialKey KeyEnter) Down _ _) (Controlador Jogar, jogo) = (ModoJogo jogo, jogo)
event (EventKey (SpecialKey KeyUp) Down _ _) (Controlador Jogar, jogo) = (Controlador Sair, jogo)
event (EventKey (SpecialKey KeyDown) Down _ _) (Controlador Jogar, jogo) = (Controlador Sair, jogo)
event (EventKey (SpecialKey KeyUp) Down _ _) (Controlador Sair, jogo) = (Controlador Jogar, jogo)
event (EventKey (SpecialKey KeyDown) Down _ _) (Controlador Sair, jogo) = (Controlador Jogar, jogo)
event (EventKey (SpecialKey KeyEnter) Down _ _) (Controlador Sair, jogo) = undefined
event (EventKey (SpecialKey KeyEnter) Down _ _) (VenceuJogo, jogo) = (Controlador Jogar, jogo)
event _ (ModoJogo (Jogo (x, y) []), jogo) = (VenceuJogo, jogo)
event (EventKey (SpecialKey KeyUp) Down _ _) (ModoJogo (Jogo (x, y) l), jogo) = (ModoJogo $ engine (x, y + 50) l, jogo)
event (EventKey (SpecialKey KeyDown) Down _ _) (ModoJogo (Jogo (x, y) l), jogo) = (ModoJogo $ engine (x, y - 50) l, jogo)
event (EventKey (SpecialKey KeyLeft) Down _ _) (ModoJogo (Jogo (x, y) l), jogo) = (ModoJogo $ engine (x - 50, y) l, jogo)
event (EventKey (SpecialKey KeyRight) Down _ _) (ModoJogo (Jogo (x, y) l), jogo) = (ModoJogo $ engine (x + 50, y) l, jogo)
event _ w = w

time :: Float -> World -> World
time _ w = w

estado :: World
estado = (Controlador Jogar, Jogo (200, 100) [(50, 50), (-250, -100), (-100, -50)])

main :: IO ()
main = do
  play window white fr estado draw event time
