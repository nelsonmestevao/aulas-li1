module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import System.Exit

data Jogo =
  Jogo (Int, Int) [(Int, Int)]

data Opcoes
  = Jogar
  | Sair

data Menu
  = Controlador Opcoes
  | ModoJogo Jogo
  | VenceuJogo

data Imagens =
  Imagens
    { pacman :: Picture
    }

type World = (Menu, Jogo, Imagens)

window :: Display
window = InWindow "Pacman" (720, 400) (0, 0)

fr :: Int
fr = 25

draw :: World -> IO Picture
draw (VenceuJogo, jogo, imgs) =
  return $ Translate (-200) 0 $ Color red $ Text "Ganhou"
draw (Controlador Jogar, jogo, imgs) =
  return $
  Pictures
    [Color blue $ drawOption "Jogar", Translate (0) (-70) $ drawOption "Sair"]
draw (Controlador Sair, jogo, imgs) =
  return $
  Pictures
    [drawOption "Jogar", Color blue $ Translate (0) (-70) $ drawOption "Sair"]
draw (ModoJogo (Jogo (x, y) l), jogo, imgs) =
  return $
  Pictures $
  (map drawSmallCircles l) ++ [Translate i j $ Color green (pacman imgs)]
  where
    i = fromIntegral x
    j = fromIntegral y

drawOption option = Translate (-50) 0 $ Scale (0.5) (0.5) $ Text option

drawSmallCircles :: (Int, Int) -> Picture
drawSmallCircles (x, y) = Translate i j $ Circle 5
  where
    i = fromIntegral x
    j = fromIntegral y

engine :: (Int, Int) -> [(Int, Int)] -> Jogo
engine p l = Jogo p (filter (p /=) l)

event :: Event -> World -> IO World
event (EventKey (SpecialKey KeyEnter) Down _ _) (Controlador Jogar, jogo, imgs) =
  return (ModoJogo jogo, jogo, imgs)
event (EventKey (SpecialKey KeyUp) Down _ _) (Controlador Jogar, jogo, imgs) =
  return (Controlador Sair, jogo, imgs)
event (EventKey (SpecialKey KeyDown) Down _ _) (Controlador Jogar, jogo, imgs) =
  return (Controlador Sair, jogo, imgs)
event (EventKey (SpecialKey KeyUp) Down _ _) (Controlador Sair, jogo, imgs) =
  return (Controlador Jogar, jogo, imgs)
event (EventKey (SpecialKey KeyDown) Down _ _) (Controlador Sair, jogo, imgs) =
  return (Controlador Jogar, jogo, imgs)
event (EventKey (SpecialKey KeyEnter) Down _ _) (Controlador Sair, jogo, imgs) =
  exitSuccess
event (EventKey (SpecialKey KeyEnter) Down _ _) (VenceuJogo, jogo, imgs) =
  return (Controlador Jogar, jogo, imgs)
event _ (ModoJogo (Jogo (x, y) []), jogo, imgs) =
  return (VenceuJogo, jogo, imgs)
event (EventKey (SpecialKey KeyUp) Down _ _) (ModoJogo (Jogo (x, y) l), jogo, imgs) =
  return (ModoJogo $ engine (x, y + 50) l, jogo, imgs)
event (EventKey (SpecialKey KeyDown) Down _ _) (ModoJogo (Jogo (x, y) l), jogo, imgs) =
  return (ModoJogo $ engine (x, y - 50) l, jogo, imgs)
event (EventKey (SpecialKey KeyLeft) Down _ _) (ModoJogo (Jogo (x, y) l), jogo, imgs) =
  return (ModoJogo $ engine (x - 50, y) l, jogo, imgs)
event (EventKey (SpecialKey KeyRight) Down _ _) (ModoJogo (Jogo (x, y) l), jogo, imgs) =
  return (ModoJogo $ engine (x + 50, y) l, jogo, imgs)
event _ w = return w

time :: Float -> World -> IO World
time _ = return

loadImages :: IO Imagens
loadImages = do
  pacman <- loadBMP "pacman.bmp"
  return (Imagens pacman)

main :: IO ()
main = do
  imagens <- loadImages
  let estado =
        ( Controlador Jogar
        , Jogo (200, 100) [(50, 50), (-250, -100), (-100, -50)]
        , imagens)
  playIO window white fr estado draw event time
