module Exemplo_Spec where

import Exemplo (fact, fib)
import Test.HUnit

fact_test01 :: Test
-- fact_test01 = TestCase (assertEqual "4! é 24" 24 (fact 4))
fact_test01 = "4! é 24 " ~: 24 ~=? fact 4

fact_test02 :: Test
fact_test02 = TestCase (assertEqual "0! é 1" 1 (fact 0))

fact_test03 :: Test
fact_test03 = TestCase (assertEqual "5! é 5*4!" 120 (5 * fact 4))

factTests = TestList [fact_test01, fact_test02, fact_test03]

fib_test01 :: Test
fib_test01 = TestCase (assertEqual "fib 1 é 1" 1 (fib 1))

fib_test02 :: Test
fib_test02 = TestCase (assertEqual "fib 4 é 5" 5 (fib 4))

fibTests = TestList [fib_test01, fib_test02]

tests = TestList [factTests, fibTests]
