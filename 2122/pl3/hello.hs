module Hello where

hello :: String
hello = "Hello, World! 👋"

main = putStrLn hello
