module Ficha1 where

area :: Double -> Double
area lado = lado ^ 2

perimetro :: Double -> Double -> Double
perimetro comprimento largura = 2 * comprimento + 2 * largura

pertence :: Char -> String -> Bool
pertence c "" = False
pertence c str =
  if c == head str
    then True
    else pertence c (tail str)

{- (d) Defina uma função que recebe uma lista não vazia e que remove
o primeiro elemento da lista, se ela tiver um número par de ele-
mentos. Se tiver um número impar de elementos, remove o último
elemento da lista. -}
removeElemLista :: [a] -> [a]
removeElemLista [] = []
removeElemLista x =
  if isEven
    then tail x
    else init x
  where
    tamanho = length x
    isEven = mod tamanho 2 == 0
{-(e) Defina uma função que recebe uma lista não vazia e devolve um
par com o primeiro e o último elemento da lista.-}
-- priUlt :: [a] -> (a, a)
-- priUlt
-- some xs = foldr (+) 0 xs
-- some (x:xs) = x+some xs
