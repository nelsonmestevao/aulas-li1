type Nome = String

type Coordenada = (Int, Int)

data Movimento
  = N
  | S
  | E
  | W
  deriving (Show, Eq) -- norte, sul, este, oeste

type Movimentos = [Movimento]

data PosicaoPessoa =
  Pos Nome Coordenada
  deriving (Show, Eq)

pessoasNorte :: [PosicaoPessoa] -> [Nome]
pessoasNorte pessoas = pessoasNorteAux pessoas maxY
  where
    maxY = coordenadaMaisNorte pessoas

pessoasNorteAux :: [PosicaoPessoa] -> Int -> [Nome]
pessoasNorteAux [] _ = []
pessoasNorteAux (Pos n (_, y):xs) maxY =
  if y == maxY
    then n : pessoasNorteAux xs maxY
    else pessoasNorteAux xs maxY

-- | Encontra a maior coordenada positiva, ou seja, a coordenada mais a norte de uma lista de pessoas.
coordenadaMaisNorte :: [PosicaoPessoa] -> Int
coordenadaMaisNorte [Pos _ (_, y)] = y
coordenadaMaisNorte (Pos _ (_, y):xs) =
  if y > coordenadaMaisNorte xs
    then y
    else coordenadaMaisNorte xs
