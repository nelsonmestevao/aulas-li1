splitAlg :: Integer -> [Integer]
splitAlg x =
  if x < 10
    then  [x]
    else (splitAlg (div x 10)) ++ [(mod x 10)]

intToStr :: Integer -> String
intToStr n = aux (splitAlg n)

aux [] = []
aux (i:is) = (show i) ++ aux is

cena :: Integer -> String
cena x = aux2 "" x

aux2 :: String -> Integer -> String
aux2 str x = if x < 10
                then str ++ show x
                else aux2 (str) (div x 10) ++(show (mod x 10))
