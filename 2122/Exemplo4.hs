import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

type Estado = ([Key], (Float, Float))

type EstadoGloss = (Estado, (Picture, Picture, Float))

estadoInicial :: Estado
estadoInicial = ([], (0, 0))

estadoGlossInicial :: Picture -> Picture -> EstadoGloss
estadoGlossInicial p1 p2 = (estadoInicial, (p1, p2, 0.0))

reageEventoGloss :: Event -> EstadoGloss -> EstadoGloss
reageEventoGloss (EventKey k Down _ _) (([], (x, y)), e) = (([k], (x, y)), e)
reageEventoGloss (EventKey k Up _ _) (([_], (x, y)), e) = (([], (x, y)), e)
reageEventoGloss _ s = s -- ignora qualquer outro evento

reageTempoGloss :: Float -> EstadoGloss -> EstadoGloss
reageTempoGloss n (([], (x, y)), (p1, p2, b)) = (([], (x, y)), (p1, p2, b + n))
reageTempoGloss n (([k], (x, y)), (p1, p2, b)) =
  case k of
    (SpecialKey KeyDown) -> (([k], (x, y + 5)), (p1, p2, b + n))
    (SpecialKey KeyUp) -> (([k], (x, y - 5)), (p1, p2, b + n))
    (SpecialKey KeyLeft) -> (([k], (x - 5, y)), (p1, p2, b + n))
    (SpecialKey KeyRight) -> (([k], (x + 5, y)), (p1, p2, b + n))

fr :: Int
fr = 50

dm :: Display
dm = InWindow "Novo Jogo" (400, 400) (0, 0)

desenhaEstadoGloss :: EstadoGloss -> Picture
desenhaEstadoGloss ((_, (x, y)), (p1, p2, b))
  | mod (milissecondsPassed) 200 < 100 = Translate x y p1
  | otherwise = Translate x y p2
  where
    milissecondsPassed = round (b * 1000)

main :: IO ()
main = do
  p1 <- loadBMP "pac_open.bmp"
  p2 <- loadBMP "pac_closed.bmp"
  play
    dm -- janela onde irá correr o jogo
    (greyN 0.5) -- côr do fundo da janela
    fr -- frame rate
    (estadoGlossInicial p1 p2) -- estado inicial
    desenhaEstadoGloss -- desenha o estado do jogo
    reageEventoGloss -- reage a um evento
    reageTempoGloss -- reage ao passar do tempo
