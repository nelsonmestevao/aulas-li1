module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

type World = (Menu, Jogo)

data Menu = Initial Int
          | ModoJogo
          | VenceuJogo

data Jogo = Jogo ((Int, Int), Bool) [(Int, Int)]

initialState :: World
initialState = (ModoJogo, Jogo ((0,-250), False) [(100, 100), (-50,200), (250, 0)])

fr :: Int
fr = 25

window :: Display
window = InWindow "Square Guy" (600, 600) (0, 0)

event :: Event -> World -> World
event _ (ModoJogo,(Jogo ((x, y), bool) [])) = (VenceuJogo, Jogo ((10,20), False) [(100,100), (250, -100), (-100,-200)])
event (EventKey (SpecialKey KeyUp) Down _ _) (_,(Jogo ((x, y), bool) l)) = (ModoJogo, verificaPacman (Jogo ((x, y + 50),bool) l))
event (EventKey (SpecialKey KeyDown) Down _ _)(_, (Jogo ((x, y), bool) l)) = (ModoJogo, verificaPacman (Jogo ((x, y - 50),bool) l))
event (EventKey (SpecialKey KeyLeft) Down _ _) (_,(Jogo ((x, y), bool) l )) = (ModoJogo, verificaPacman ( Jogo ((x - 50, y), bool) l))
event (EventKey (SpecialKey KeyRight) Down _ _) (_,(Jogo ((x,y),bool) l)) = (ModoJogo, verificaPacman ( Jogo ((x + 50, y), bool) l))
event (EventKey (SpecialKey KeySpace ) Down _ _) (_,(Jogo ((x,y),bool) l)) = (ModoJogo, verificaPacman ( Jogo ((x, y), not bool) l))
event _ w = w

verificaPacman :: Jogo -> Jogo
verificaPacman w@(Jogo ((x,y), False) cs) = Jogo ((x,y), False) (filter ((x,y)/=) cs)
verificaPacman w@(Jogo (_, True) _) = w

time :: Float -> World -> World
-- time _ (Jogo ((x,y), bool) cs) = Jogo ((x,y), not bool) cs
time _ w = w

draw :: World -> Picture
draw (VenceuJogo, _) = text "Ganhou"
draw (ModoJogo, (Jogo ((x,y), bool) cs)) = Pictures $ pacman:drawCircles cs
    where
        pacman = if bool then Translate i j $ Color red $ circleSolid 20
                    else Translate i j $ Color red $ Circle 20
        i = fromIntegral x
        j = fromIntegral y
draw (_, _) = Blank

drawCircles :: [(Int, Int)] -> [Picture]
drawCircles cs = map drawCircle cs
    where
        drawCircle :: (Int, Int) -> Picture
        drawCircle (x, y) = Translate (fromIntegral x) (fromIntegral y) $ Color blue $ circleSolid 10

main :: IO ()
main = do
    p1 <- loadBMP "pac_open.bmp"
    p2 <- loadBMP "pac_closed.bmp"
    play window                     -- janela onde irá correr o jogo
      white               -- côr do fundo da janela
      fr                        -- frame rate
      initialState -- estado inicial
      draw        -- desenha o estado do jogo
      event          -- reage a um evento
      time           -- reage ao passar do tempo