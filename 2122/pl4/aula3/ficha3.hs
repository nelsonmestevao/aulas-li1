module Ficha3 where

type Nome = String
type Coordenada = (Int, Int)
data Movimento= N | S | E | W deriving (Show,Eq) -- norte, sul, este, oeste
type Movimentos = [Movimento]
data PosicaoPessoa = Pos Nome Coordenada deriving (Show,Eq)

posicao :: PosicaoPessoa -> Movimentos -> PosicaoPessoa
posicao (Pos n (x, y)) [] = Pos n (x, y)
-- posicao (Pos n (x,y)) (N:t) = posicao (Pos n (x, y+1)) t
-- posicao (Pos n (x,y)) (S:t) = posicao (Pos n (x, y-1)) t
-- posicao (Pos n (x,y)) (E:t) = posicao (Pos n (x+1, y)) t
-- posicao (Pos n (x,y)) (W:t) = posicao (Pos n (x-1, y)) t
posicao (Pos n (x,y)) (h:t) = case h of
    N -> posicao (Pos n (x, y+1)) t
    S -> posicao (Pos n (x, y-1)) t
    E -> posicao (Pos n (x+1, y)) t
    W -> posicao (Pos n (x-1, y)) t

posicoesM:: [PosicaoPessoa] -> Movimento -> [PosicaoPessoa]
posicoesM [] m = []
posicoesM (p:ps) m = (posicao p [m]): posicoesMs ps m

posicoesMs:: [PosicaoPessoa] -> Movimentos -> [PosicaoPessoa]
posicoesMs [] ms = []
posicoesMs (p:ps) ms = (posicao p ms): posicoesMs ps ms
