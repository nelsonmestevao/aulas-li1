module Exemplo where

isEven :: Integer -> Bool
isEven n = mod n 2 == 0

fatorial :: Integer -> Integer
fatorial 0 = 1
fatorial n = n * fatorial (n-1)
