module Exemplo_Spec where

import Test.HUnit

import Exemplo

test_isEven01 = TestCase (assertBool "4 é par" (isEven 4))
test_isEven02 = TestCase (assertBool "3 não é par" (not (isEven 3)))

--test_fatorial01 = TestCase (assertEqual "4! == 24" 24 (fatorial 4))
test_fatorial01 = "4! == 24" ~: 24 ~=? fatorial 4
test_fatorial02 = TestCase (assertEqual "5! == 120" 120 (5 * fatorial 4))

testsIsEven = TestList [test_isEven01, test_isEven02]

testsFatorial = TestList [test_fatorial01, test_fatorial02]

tests = TestList [testsFatorial, testsIsEven]

runT = runTestTT tests