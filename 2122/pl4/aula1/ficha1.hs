module Ficha1 where

{- |
Calcular a área de um quadrado com apenas o /comprimento/ de um dos lados.
Um quadro tem sempre lado maior do que zero.

== Exemplos de utilização:
>>> area 5
25.0
>>> area 1
1.0

== Propriedades

prop> area n > 0
-}
area :: Double -> Double
area l = l ^ 2

-- (b) Defina uma função que calcule o perímetro de um rectângulo, dados o
-- comprimento e a largura.
perimetro :: Double -> Double -> Double
perimetro lado largura = lado * 2 + largura * 2

-- (c) Defina uma função que verifique se um caractere pertence a uma string.
pertence :: Char -> [Char] -> Bool
pertence c [] = False
pertence c (x:xs)
  | c == x = True
  | otherwise = pertence c xs

-- (d) Defina uma função que recebe uma lista não vazia e que remove o primeiro
-- elemento da lista, se ela tiver um número par de elementos. Se tiver um
-- número impar de elementos, remove o último elemento da lista.
popList :: [a] -> [a]
popList l =
  if isEven (length l)
    then tail l
    else init l
  where
    isEven :: Int -> Bool
    isEven x = mod x 2 == 0 -- ou então função predefinida `even x`

-- (f) Defina uma função que recebe uma lista de strings, representando os nomes
-- de uma pessoa, e produz um par com o primeiro e o último nome dessa pessoa.
-- ["nelson", "miguel", "estevao"]
-- ("nelson", "estevao)
nome :: [[Char]] -> ([Char], [Char])
nome list = (head list, last list)

-- (h) Defina uma função que recebe uma lista de nomes de uma pessoa (uma lista
-- de valores do tipo string) e produz uma string com a inicial do primeiro
-- nome, seguida do caracter ’.’ seguida do úl- timo nome. Por exemplo, se a
-- função receber a lista ["Joaquim", "Francisco", "Alves", "Martins"], deve
-- devolver o valor "J.Martins"
nickname :: [String] -> String
nickname nomes = primeira_letra : "." ++ apelido
  where
    primeira_letra = head (head nomes)
    apelido = last nomes
