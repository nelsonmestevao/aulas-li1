module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Data.Maybe

data Jogo = Jogo (Int, Int) [(Int, Int)]

data Opcoes = Jogar
            | Sair

data Menu = Controlador Opcoes
          | ModoJogo Jogo Float
          | VenceuJogo

data Pacman = Open | Closed
    deriving Eq

type Imagens = [(Pacman, Picture)]

type World = (Menu, Jogo, Imagens)

window :: Display
window = InWindow "Pacman" (720, 400) (0, 0)

fr :: Int
fr = 25

draw :: World -> Picture
draw (VenceuJogo, jogo, imagens) = Translate (-200) 0 $ Color red $ Text "Ganhou"
draw (Controlador Jogar, jogo, imagens) = Pictures [Color blue $ drawOption "Jogar", Translate (0) (-70) $ drawOption "Sair"]
draw (Controlador Sair, jogo, imagens) = Pictures [drawOption "Jogar", Color blue $ Translate (0) (-70) $ drawOption "Sair"]
draw (ModoJogo (Jogo (x, y) l) n, jogo, imagens) = Pictures $ (map drawSmallCircles l) ++ [Translate i j $ Color green pacman]
  where
      i = fromIntegral x
      j = fromIntegral y
      pacman = if even (round n) then fromJust (lookup Open imagens) else fromJust (lookup Closed imagens)

drawOption option = Translate (-50) 0 $ Scale (0.5) (0.5) $ Text option

drawSmallCircles :: (Int, Int) -> Picture
drawSmallCircles (x, y) = Translate i j $ Circle 5
    where
      i = fromIntegral x
      j = fromIntegral y

engine :: (Int, Int) -> [(Int, Int)] -> Jogo
engine p l = Jogo p (filter (p/=) l)

event :: Event -> World -> World
event (EventKey (SpecialKey KeyEnter) Down _ _) (Controlador Jogar, jogo, imagens) = (ModoJogo jogo 0, jogo, imagens)
event (EventKey (SpecialKey KeyUp) Down _ _) (Controlador Jogar, jogo, imagens) = (Controlador Sair, jogo, imagens)
event (EventKey (SpecialKey KeyDown) Down _ _) (Controlador Jogar, jogo, imagens) = (Controlador Sair, jogo, imagens)
event (EventKey (SpecialKey KeyUp) Down _ _) (Controlador Sair, jogo, imagens) = (Controlador Jogar, jogo, imagens)
event (EventKey (SpecialKey KeyDown) Down _ _) (Controlador Sair, jogo, imagens) = (Controlador Jogar, jogo, imagens)
event (EventKey (SpecialKey KeyEnter) Down _ _) (Controlador Sair, jogo, imagens) = undefined
event (EventKey (SpecialKey KeyEnter) Down _ _) (VenceuJogo, jogo, imagens) = (Controlador Jogar, jogo, imagens)
event _ (ModoJogo (Jogo (x, y) []) n, jogo, imagens) = (VenceuJogo, jogo, imagens)
event (EventKey (SpecialKey KeyUp) Down _ _) (ModoJogo (Jogo (x, y) l) n, jogo, imagens) = (ModoJogo ( engine (x, y + 50) l) n, jogo, imagens)
event (EventKey (SpecialKey KeyDown) Down _ _) (ModoJogo (Jogo (x, y) l) n, jogo, imagens) = (ModoJogo ( engine (x, y - 50) l) n, jogo, imagens)
event (EventKey (SpecialKey KeyLeft) Down _ _) (ModoJogo (Jogo (x, y) l) n, jogo, imagens) = (ModoJogo ( engine (x - 50, y) l) n, jogo, imagens)
event (EventKey (SpecialKey KeyRight) Down _ _) (ModoJogo (Jogo (x, y) l) n, jogo, imagens) = (ModoJogo ( engine (x + 50, y) l) n, jogo, imagens)
event _ w = w

time :: Float -> World -> World
time n (ModoJogo (Jogo (x, y) l) i, jogo, imagens) = (ModoJogo (Jogo (x, y) l) (i+n), jogo, imagens)
time _ w = w

main :: IO ()
main = do
  pacman_open <- loadBMP "pac_open.bmp"
  pacman_closed <- loadBMP "pac_closed.bmp"

  let imagens = [(Open, pacman_open), (Closed, pacman_closed)]

  let estado = (Controlador Jogar, Jogo (200, 100) [(50, 50), (-250, -100), (-100, -50)], imagens)

  play window white fr estado draw event time
