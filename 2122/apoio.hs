module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

square = polygon [(-25, -25), (-25, 25), (25, 25), (25, -25), (-25, -25)]

data Cobra =
  Cobra
    { cabeça :: (Int, Int)
    , cauda :: [(Int, Int)]
    }

data Jogo =
  Jogo
    { cobra :: Cobra
    , maçãs :: [(Int, Int)]
    }

type World = Jogo

window :: Display
window = InWindow "Comilão" (1280, 720) (0, 0)

fr :: Int
fr = 25

initialState :: World
initialState = Jogo (Cobra (0, 0) []) [(200, 100), (400, 300), (-200, -300)]

draw :: World -> Picture
draw (Jogo (Cobra (x, y) cauda) maçãs) =
  Pictures $
  (Translate i j $ Color green $ square) :
  (map drawTronco cauda) ++ map drawMaçã maçãs
  where
    i = fromIntegral x
    j = fromIntegral y

drawTronco :: (Int, Int) -> Picture
drawTronco (x, y) = Translate i j $ Color orange $ square
  where
    i = fromIntegral x
    j = fromIntegral y

drawMaçã :: (Int, Int) -> Picture
drawMaçã (x, y) = Translate i j $ Color red $ circleSolid 5
  where
    i = fromIntegral x
    j = fromIntegral y

engine :: Jogo -> (Int, Int) -> Jogo
engine (Jogo (Cobra (x, y) []) maçãs) (a, b)
  | (a, b) `elem` maçãs =
    Jogo (Cobra (a, b) [(x, y)]) (filter ((a, b) /=) maçãs)
  | otherwise = Jogo (Cobra (a, b) []) maçãs
engine (Jogo (Cobra (x, y) cauda) maçãs) (a, b)
  | (a, b) `elem` maçãs =
    Jogo
      (Cobra (a, b) ((x, y) : (init cauda) ++ [(x, y)]))
      (filter ((a, b) /=) maçãs)
  | otherwise =
    Jogo (Cobra (a, b) ((x, y) : init cauda)) (filter ((a, b) /=) maçãs)

event :: Event -> World -> World
event (EventKey (SpecialKey KeyUp) Down _ _) jogo@(Jogo (Cobra (x, y) _) _) =
  engine jogo (x, y + 50)
event (EventKey (SpecialKey KeyDown) Down _ _) jogo@(Jogo (Cobra (x, y) _) _) =
  engine jogo (x, y - 50)
event (EventKey (SpecialKey KeyRight) Down _ _) jogo@(Jogo (Cobra (x, y) _) _) =
  engine jogo (x + 50, y)
event (EventKey (SpecialKey KeyLeft) Down _ _) jogo@(Jogo (Cobra (x, y) _) _) =
  engine jogo (x - 50, y)
event _ w = w

time :: Float -> World -> World
time _ w = w

main :: IO ()
main = play window white fr initialState draw event time
