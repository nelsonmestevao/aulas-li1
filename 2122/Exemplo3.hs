module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Graphics.Gloss.Juicy (loadJuicy)

type Estado = (Float, Float)

-- estado inicial, semelhante ao anterior mas duas imagens 
-- e um valor de segundos passados desde o inicio do programa
type EstadoGloss = (Estado, (Picture, Picture, Float))

estadoInicial :: Estado
estadoInicial = (0, 0)

estadoGlossInicial :: Picture -> Picture -> EstadoGloss
estadoGlossInicial p1 p2 = (estadoInicial, (p1, p2, 0.0))

reageEventoGloss :: Event -> EstadoGloss -> EstadoGloss
reageEventoGloss (EventKey (SpecialKey KeyUp) Down _ _) ((x, y), e) =
  ((x, y + 5), e)
reageEventoGloss (EventKey (SpecialKey KeyDown) Down _ _) ((x, y), e) =
  ((x, y - 5), e)
reageEventoGloss (EventKey (SpecialKey KeyLeft) Down _ _) ((x, y), e) =
  ((x - 5, y), e)
reageEventoGloss (EventKey (SpecialKey KeyRight) Down _ _) ((x, y), e) =
  ((x + 5, y), e)
reageEventoGloss _ s = s -- ignora qualquer outro evento 

reageTempoGloss :: Float -> EstadoGloss -> EstadoGloss
reageTempoGloss n ((x, y), (p1, p2, b)) = ((x, y), (p1, p2, b + n))

fr :: Int
fr = 50

dm :: Display
dm = InWindow "Novo Jogo" (400, 400) (0, 0)

-- alternar a imagem a cada 200 milissegundos
desenhaEstadoGloss :: EstadoGloss -> Picture
desenhaEstadoGloss ((x, y), (p1, p2, b))
  | mod (milissecondsPassed) 200 < 100 = Translate x y p1
  | otherwise = Translate x y p2
  where
    milissecondsPassed = round (b * 1000)

main :: IO ()
main
    --p <- loadBMP "imagem.bmp"
 = do
  Just p1 <- loadJuicy "pac_open.bmp"
  Just p2 <- loadJuicy "pac_closed.bmp"
  play
    dm -- janela onde irá correr o jogo
    (greyN 0.5) -- côr do fundo da janela
    fr -- frame rate
    (estadoGlossInicial p1 p2) -- estado inicial
    desenhaEstadoGloss -- desenha o estado do jogo
    reageEventoGloss -- reage a um evento
    reageTempoGloss -- reage ao passar do tempo
