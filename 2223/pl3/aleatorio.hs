module Aleatorio where

-- $ cabal update
-- $ cabal install random
-- $ ghci -package random aleatorio.hs

import           System.Random

type Comprimento = Int
type Semente = Int

geraListaAleatorios :: Semente -> Comprimento -> [Int]
geraListaAleatorios s c = take c $ randoms (mkStdGen s)
