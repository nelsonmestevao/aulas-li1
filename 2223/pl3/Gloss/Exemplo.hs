module Exemplo where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

type Posicao = (Int, Int)

data Pacman
  = Pacman Posicao Direcao

type Ponto = Posicao

data Jogo
  = Jogo Pacman [Ponto]

data Direcao
  = Cima
  | Baixo
  | Direita
  | Esquerda
  | Parado
  deriving (Eq)

-- data Menu
--   = Jogar
--   | Menu Int
--   | GameOver
-- type World = (Menu, Jogo)
type World = Jogo

desenha :: World -> IO Picture
desenha (Jogo pacman pontos) =
  return $ Pictures $ map desenhaPonto pontos ++ [desenhaPacman pacman]

desenhaPacman :: Pacman -> Picture
desenhaPacman (Pacman (i, j) direcao) =
  Translate x y $ Color yellow $ circleSolid 20
  where
    x = fromIntegral i
    y = fromIntegral j

desenhaPonto :: Ponto -> Picture
desenhaPonto (i, j) = Translate x y $ Color red $ Circle 5
  where
    x = fromIntegral i
    y = fromIntegral j

atualizaJogo :: Jogo -> Jogo
atualizaJogo (Jogo (Pacman (x, y) direcao) pontos) =
  Jogo (Pacman (x, y) direcao) (filter (/= (x, y)) pontos)

evento :: Event -> World -> IO World
evento (EventKey (SpecialKey KeyUp) Down _ _) (Jogo (Pacman posicao _) pontos) =
  return $ Jogo (Pacman posicao Cima) pontos
evento (EventKey (SpecialKey KeyDown) Down _ _) (Jogo (Pacman posicao _) pontos) =
  return $ Jogo (Pacman posicao Baixo) pontos
evento (EventKey (SpecialKey KeyLeft) Down _ _) (Jogo (Pacman posicao _) pontos) =
  return $ Jogo (Pacman posicao Esquerda) pontos
evento (EventKey (SpecialKey KeyRight) Down _ _) (Jogo (Pacman posicao _) pontos) =
  return $ Jogo (Pacman posicao Direita) pontos
evento (EventKey (SpecialKey KeyUp) Up _ _) (Jogo (Pacman posicao Cima) pontos) =
  return $ Jogo (Pacman posicao Parado) pontos
evento (EventKey (SpecialKey KeyDown) Up _ _) (Jogo (Pacman posicao Baixo) pontos) =
  return $ Jogo (Pacman posicao Parado) pontos
evento (EventKey (SpecialKey KeyLeft) Up _ _) (Jogo (Pacman posicao Esquerda) pontos) =
  return $ Jogo (Pacman posicao Parado) pontos
evento (EventKey (SpecialKey KeyRight) Up _ _) (Jogo (Pacman posicao Direita) pontos) =
  return $ Jogo (Pacman posicao Parado) pontos
evento _ w = return w

tempo :: Float -> World -> IO World
tempo _ jogo@(Jogo (Pacman (x, y) direcao) pontos) =
  case direcao of
    Cima -> return $ atualizaJogo $ Jogo (Pacman (x, y + 20) Cima) pontos
    Baixo -> return $ atualizaJogo $ Jogo (Pacman (x, y - 20) Baixo) pontos
    Direita -> return $ atualizaJogo $ Jogo (Pacman (x + 20, y) Direita) pontos
    Esquerda -> return $ atualizaJogo $ Jogo (Pacman (x - 20, y) Esquerda) pontos
    Parado -> return jogo

janela :: Display
janela = InWindow "Pacman da Wish" (800, 800) (0, 0)

fr :: Int
fr = 15

estadoInicial :: World
estadoInicial =
  Jogo (Pacman (0, 0) Parado) [(200, 200), (300, 300), (80, 300), (-100, 200)]

main = playIO janela white fr estadoInicial desenha evento tempo
