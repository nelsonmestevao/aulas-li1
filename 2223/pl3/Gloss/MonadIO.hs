module MonadIO where

import System.Random

aleatorio :: IO Int
aleatorio = do
  x <- randomRIO (0, 100)
  y <- randomRIO (0, 100)
  putStrLn (show x ++ ":" ++ show y)
  return (x * y)