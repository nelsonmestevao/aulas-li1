module Ficha2 where

type Nome = String

type Coordenada = (Int, Int)

data Movimento
  = N -- ^ Norte
  | S -- ^ Sul
  | E -- ^ Este
  | W -- ^ Oeste
  deriving (Show, Eq)

type Movimentos = [Movimento]

data PosicaoPessoa =
  Pos Nome Coordenada
  deriving (Show, Eq)

posicao :: PosicaoPessoa -> Movimentos -> PosicaoPessoa
posicao (Pos y (a, b)) [] = Pos y (a, b)
posicao (Pos y (a, b)) (c:d) =
  case c of
    N -> posicao (Pos y (a, b + 1)) d
    S -> posicao (Pos y (a, b - 1)) d
    E -> posicao (Pos y (a + 1, b)) d
    W -> posicao (Pos y (a - 1, b)) d

{-
(c) Dada uma lista de posições de pessoas, actialize essa lista depois de todas
as pessoas executarem uma mesma sequência de movimentos. Use as funções
anteriormente definidas.
-}
posicoesMs :: [PosicaoPessoa] -> Movimentos -> [PosicaoPessoa]
posicoesMs ps []    = ps
posicoesMs (p:ps) m = posicao p m : posicoesMs ps m
