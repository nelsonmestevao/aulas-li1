module Fact_Spec where

import           Fact
import           Test.HUnit

factTests =
  test
    [ "fact 0" ~: 1 ~=? fact 0,
      "fact 1" ~: 1 ~=? fact 1,
      "fact 4" ~: 24 ~=? fact 4,
      "fact 5" ~: 120 ~=? fact 5
    ]
