-- |
-- Module: Fact
-- Description: Factorial related functions
-- Copyright: Nelson Estevão <d12733@di.uminho.pt>
--
-- Factorial is a really important function in mathematics and usually we use the `!` operator.
module Fact where

-- | Função que calcula o factoria de um número natural (incluindo 0).
--
-- == Exemplos de utilização:
--
-- >>> fact 0
-- 1
--
-- >>> fact 5
-- 120
fact :: Int -> Int
fact 0 = 1
fact n = n * fact (n - 1)
