# 2022/23

| Data      | Aula                     | Tópicos                          |     |     |
| --------- | ------------------------ | -------------------------------- | --- | --- |
| ~~19/9~~  | Ficha 0                  | Instalação de Software           |     |     |
| 26/9      | Ficha 1                  | Unix + Haskell                   |     |     |
| 3/10      | Ficha 2                  | Git + GitLab + Haskell           |     |     |
| 10/10     | Ficha 3 & 4              | GitLab + Haddock + Haskell       |     |     |
| 17/10     | Ficha 5                  | HUnit + Haskell                  |     |     |
| 24/10     | Prova Individual + Apoio |                                  |     |     |
| ~~31/10~~ | ==Testes Intercalares==  |                                  |     |     |
| 7/11      | Ficha 6 + Apoio          | Discussão de qualidade do código |     |     |
| 14/11     | Ficha 7 + Apoio          | Gloss + Haskell                  |     |     |
| 21/11     | Ficha 8 + Apoio          | Gloss + Haskell                  |     |     |
| 28/11     | Ficha 9 + Apoio          |                                  |     |     |
| 5/12      | Apoio                    |                                  |     |     |
| 12/12     | Apoio                    |                                  |     |     |

### Aula 1

_<time>2022/09/26</time>_

- UNIX
- Haskell

### Aula 2

_<time>2022/10/03</time>_

- Git
- Haskell

### Aula 3

_<time>2022/10/10</time>_

- GitLab
- Haddock
- Haskell

### Aula 4

_<time>2022/10/17</time>_

- HUnit
- Haskell

### Aula 5

_<time>2022/10/24</time>_

- Apoio ao Projecto

### Aula 6

_<time>2022/11/07</time>_

- Apoio ao Projeto

### Aula 7

_<time>2022/11/14</time>_

- Gloss
- Haskell
- Apoio ao Projeto

### Aula 8

_<time>2022/11/21</time>_

- Gloss
- Haskell
- Apoio ao Projeto

### Aula 9

_<time>2022/11/28</time>_

- Apoio ao Projeto

### Aula 10

_<time>2022/12/05</time>_

- Apoio ao Projeto

### Aula 11

_<time>2022/12/12</time>_

- Apoio ao Projeto
