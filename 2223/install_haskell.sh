#!/usr/bin/env bash

curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | BOOTSTRAP_HASKELL_NONINTERACTIVE=1 sh

cabal install --lib HUnit
cabal install --lib gloss

code --install-extension haskell.haskell

