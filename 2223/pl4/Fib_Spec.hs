module Fib_Spec where

import           Fib
import           Test.HUnit

tests =
  TestLabel
    "Fib"
    $ test ["fib 0" ~: 1 ~=? fib 0, "fib 5" ~: 8 ~=? fib 5]
