module Ficha where

type Nome = String

type Coordenada = (Int, Int)

data PosicaoPessoa =
  Pos Nome Coordenada
  deriving (Show, Eq)

coordenadaMaisNorteAux :: Int -> [PosicaoPessoa] -> Int
coordenadaMaisNorteAux n [] = n
coordenadaMaisNorteAux n ((Pos _ (_, y)):ps) =
  if n >= y
    then coordenadaMaisNorteAux n ps
    else coordenadaMaisNorteAux y ps

coordenadaMaisNorte :: [PosicaoPessoa] -> Int
coordenadaMaisNorte ((Pos _ (_, y)):ps) = coordenadaMaisNorteAux y ps

pessoasNorte :: [PosicaoPessoa] -> [Nome]
pessoasNorte [] = []
pessoasNorte lista@((Pos nome (x, y)):ps)
  | y == maior = nome : pessoasNorte ps
  | otherwise = pessoasNorte ps
  where
    maior = coordenadaMaisNorte lista
