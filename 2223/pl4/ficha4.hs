{-
2. Defina uma função que recebe uma lista e desloca cada elemento da lista, n posições
   para a direita. Os elementos do final da lista são colocados no início (e.g.
   [1,2,3,4,5] daria [4,5,1,2,3], para n=2).
-}
desloca :: Int -> [a] -> [a]
desloca _ []   = []
desloca 0 list = list
desloca n list = drop (n + 1) list ++ take (n + 1) list

{-|
6. Defina uma função recursiva que procure a posição de um elemento numa lista
   (posição da primeira ocorrência). Devolve (-1) caso o elemento não ocorra na
   lista.
-}
findL' :: Eq a => Int -> a -> [a] -> Int
findL' _ _ [] = -1
findL' i x (h:t)
  | h == x = i
  | otherwise = findL' (i + 1) x t

findL :: Eq a => a -> [a] -> Int
findL = findL' 0

{-|
8. Defina uma função recursiva que procure a posição de um elemento numa matriz
   (posição da primeira ocorrência que encontrar). Use funções anteriormente
   definidas.
-}
findM' :: Eq a => (Int, Int) -> a -> [[a]] -> (Int, Int)
findM' _ _ [] = (-1, -1)
findM' (i, j) x (l:ls)
  | findL x l == (-1) = findM' (i + 1, 0) x ls
  | otherwise = (i, j + findL x l)

findM :: Eq a => a -> [[a]] -> (Int, Int)
findM = findM' (0, 0)
