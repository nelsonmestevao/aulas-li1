module Exemplo where

import Data.Maybe

data Obstaculo
  = Tronco
  | Arvore
  | Carro

type Picture = Int

data Theme
  = Dark
  | Light

type Dicionario = [(Obstaculo, Picture)]

type Imagens = [(Theme, Dicionario)]

type ImagensDark = Dicionario

type ImagensLight = Dicionario

getPicture :: Theme -> Obstaculo -> Dicionario -> Picture
getPicture theme key imgs = getValue key $ getValue theme imgs

getValue :: Obstaculo -> Picture
getValue key dict = fromJust $ lookup key dict
