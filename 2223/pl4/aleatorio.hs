module Aleatorio where

import System.Random

duplica :: Num a => a -> a
duplica x = x + x

aleatorio :: IO Int
aleatorio = do
    x <- randomRIO (0, 100)
    y <- randomRIO (0, 100)

    let c = duplica x

    putStrLn ("x:"++show x++"//y:"++show y)
    putStrLn ("c:"++show c)
    putStrLn "Terminou..."

    return (x * y)
