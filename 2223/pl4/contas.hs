import Data.List.Split

separarNaVirgula :: String -> [String]
separarNaVirgula linha = splitOn "," linha

convertListStringEmInteiros :: [String] -> [Int]
convertListStringEmInteiros numeros = map read numeros

gastos :: IO Int
gastos = do
  dados <- readFile "contas.csv"
  let numeros =
        convertListStringEmInteiros $
        map last $ map separarNaVirgula $ tail $ lines dados
  return $ sum numeros
