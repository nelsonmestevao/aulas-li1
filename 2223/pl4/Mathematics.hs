{-|
Module : Mathematics
Description : Módulo Haskell contendo exemplos de funções matematicas
Copyright : Dário Guimarães <a104344@alunos.uminho.pt>
            Nelson Estevão <d12733@di.uminho.pt>

Este módulo contém definições Haskell para o cálculo de fatoriais (obs: isto é apenas uma descrição mais
longa do módulo para efeitos de documentação...).
-}
module Mathematics where

-- | A função 'fib' retorna o @n@-ésimo elemento da sequência de Fibonacci
-- Algumas características que distinguem a função 'fib' da 'fact':
--
-- * o padrão de recursividade é binário;
-- * dispõe de dois casos base.
--
-- == Exemplos de utilização:
-- >>> fib 4
-- 3
-- >>> fib 10
-- 55
fib :: Integer -> Integer
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

fact :: Int -> Int
fact 0 = 1
fact n = n * fact (n - 1)
