module SaveState where

import Data.List.Split

data Terreno
  = Rio
  | Estrada
  | Relva
  deriving (Show)

toTerreno :: Char -> Terreno
toTerreno '-' = Estrada
toTerreno '~' = Rio
toTerreno '^' = Relva

ler :: IO [Terreno]
ler = do
  estado <- readFile "exemplo.txt"
  let linhas = lines estado
  let [x, y] = splitOn " " $ head linhas
  let i = read x :: Int
  let j = read y :: Int
  let o = map toTerreno $ map head $ tail linhas
  putStrLn ("Jogador: " ++ show i ++ "," ++ show j)
  return o
