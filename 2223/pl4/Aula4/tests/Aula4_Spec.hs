module Aula4_Spec where

import           Aula4
import           Test.HUnit

tests =
  test
    [ "fact 0" ~: 1 ~=? fact 0
    , "fact 2" ~: 2 ~=? fact 2
    , "fact 5" ~: 120 ~=? fact 5
    ]
