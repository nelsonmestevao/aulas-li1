-- | Module: Aula 4
--   Description: qualquer coisa
--   Copyright: Nelson Estevão <d12733@di.uminho.pt>
--
--   Uma descrição mais longa
module Aula4 where

-- | A função 'fact' dá-me o factorial.
--
-- == Exemplos de utilização:
--
-- >>> fact 0
-- 1
--
-- >>> fact 5
-- 120
fact :: Int -> Int
fact 0 = 1
fact n = n * fact (n - 1)
