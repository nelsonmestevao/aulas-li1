module Teste where

éDigito :: Char -> Bool
éDigito n = n `elem` ['0' .. '9']

f2 :: [Char] -> String
f2 (c:cs) =
  if éDigito c
    then f2 cs
    else c : f2 cs

f4 :: [String] -> [String]
f4 (s:ss) =
  if length s `elem` [0, 1]
    then f4 ss
    else init s : f4 ss

f4' (s:ss)
  | s == "" = f4' ss
  | length s == 1 = f4' ss
  | otherwise = init s : f4' ss
