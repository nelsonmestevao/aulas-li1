module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

type Coordenadas = (Float, Float)

type Maca = Coordenadas

type Jogador = Coordenadas

data Jogo = Jogo Jogador [Maca]

type Estado = Jogo

data Movimento = Cima | Baixo | Esquerda | Direita

movimenta :: Movimento -> Jogo -> Jogo
movimenta Cima (Jogo (x, y) macas) = Jogo (x, y + 10) macas
movimenta Baixo (Jogo (x, y) macas) = Jogo (x, y - 10) macas
movimenta Esquerda (Jogo (x, y) macas) = Jogo (x - 10, y) macas
movimenta Direita (Jogo (x, y) macas) = Jogo (x + 10, y) macas

atualiza :: Jogo -> Jogo
atualiza (Jogo (x, y) macas) = Jogo (x, y) (filter (\m -> m /= (x, y)) macas)

estadoInicial :: Jogo
estadoInicial = Jogo (200, 400) [(180, 50), (300, -200), (-200, 100)]

desenha :: Estado -> Picture
desenha (Jogo (x, y) macas) = Pictures $ desenhaJogador (x, y) : map desenhaMaca macas

desenhaJogador :: Jogador -> Picture
desenhaJogador (x, y) = Translate x y (Circle 20)

desenhaMaca :: Maca -> Picture
desenhaMaca (x, y) = Color red $ Translate x y (circleSolid 10)

-- https://hackage.haskell.org/package/gloss-1.13.2.2/docs/Graphics-Gloss-Interface-Pure-Game.html#t:Key
reage :: Event -> Estado -> Estado
reage (EventKey (SpecialKey KeyUp) Down _ (_, _)) jogo = movimenta Cima jogo
reage (EventKey (SpecialKey KeyDown) Down _ (_, _)) jogo = movimenta Baixo jogo
reage (EventKey (SpecialKey KeyLeft) Down _ (_, _)) jogo = movimenta Esquerda jogo
reage (EventKey (SpecialKey KeyRight) Down _ (_, _)) jogo = movimenta Direita jogo
reage _ e = e

tempo :: Float -> Estado -> Estado
tempo _ e = atualiza e

janela :: Display
janela = InWindow "Jogo da Cobra (versão da Wish)" (800, 800) (0, 0)

backgroundColor :: Color
backgroundColor = green

fr :: Int
fr = 15

-- https://hackage.haskell.org/package/gloss-1.13.2.2/docs/Graphics-Gloss-Interface-Pure-Game.html
main = play janela backgroundColor fr estadoInicial desenha reage tempo