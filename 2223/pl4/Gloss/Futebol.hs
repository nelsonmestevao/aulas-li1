module Futebol where

import System.Random

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

type Coordenadas = (Float, Float)

type Estado = (Coordenadas, Imagens)

data Imagens =
  Imagens
    { bola :: Picture
    , campo :: Picture
    }

desenha :: Estado -> IO Picture
desenha (coordenadas, imagens) = return $ Pictures [campo imagens, desenhaBola imagens coordenadas]

desenhaBola :: Imagens -> Coordenadas -> Picture
desenhaBola imagens (x, y) = Translate x y $ (bola imagens)

data Movimento = Cima
               | Esquerda
               | Direita
               | Baixo

movimenta :: Movimento -> Estado -> Estado
movimenta Cima ((x, y), imagens) = ((x, y + 10), imagens)
movimenta Baixo ((x, y), imagens) = ((x, y - 10), imagens)
movimenta Esquerda ((x, y), imagens) = ((x- 10, y), imagens)
movimenta Direita ((x, y), imagens) = ((x+ 10, y), imagens)

-- https://hackage.haskell.org/package/gloss-1.13.2.2/docs/Graphics-Gloss-Interface-Pure-Game.html#t:Key
reage :: Event -> Estado -> IO Estado
reage (EventKey (SpecialKey KeyUp) Down _ (_, _)) e = return $ movimenta Cima e
reage (EventKey (SpecialKey KeyDown) Down _ (_, _)) e = return $ movimenta Baixo e
reage (EventKey (SpecialKey KeyLeft) Down _ (_, _)) e = return $ movimenta Esquerda e
reage (EventKey (SpecialKey KeyRight) Down _ (_, _)) e = return $ movimenta Direita e
reage _ e = return e

tempo :: Float -> Estado -> IO Estado
tempo _ (_, imagens) = do
    x <- randomRIO (-200, 200)
    y <- randomRIO (-200, 200)
    return ((x, y), imagens)

janela :: Display
janela = InWindow "Futebol" (800, 800) (0, 0)

backgroundColor :: Color
backgroundColor = black

fr :: Int
fr = 1

carregarImagens :: IO Imagens
carregarImagens = do
  campo <- loadBMP "field.bmp"
  bola <- loadBMP "bola.bmp"
  return Imagens {bola = Scale 0.02 0.02 bola, campo = campo}

-- https://hackage.haskell.org/package/gloss-1.13.2.2/docs/Graphics-Gloss-Interface-Pure-Game.html
main = do
  imagens <- carregarImagens
  playIO janela backgroundColor fr ((0, 0), imagens) desenha reage tempo
