module Main where

import Data.Bifunctor
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

-- data Terreno = Rio | Relva | Estrada

-- type Images = [(Terreno, Picture)]

type Posição = (Int, Int)

type Maçã = Posição

type Cobra = Posição

data Jogo =
  Jogo Cobra [Maçã]

data Jogada
  = Cima
  | Baixo
  | Direita
  | Esquerda
  | Parado
  deriving (Eq)

type Estado = Jogo

estadoInicial :: Estado
estadoInicial = Jogo (200, 0) [(50, 100), (200, -400), (-400, -200)]

fromInts :: (Int, Int) -> (Float, Float)
fromInts = bimap fromIntegral fromIntegral

desenha :: Estado -> Picture
desenha (Jogo cobra maçãs) =
  Pictures $ desenhaCobra cobra : map desenhaMaçã maçãs

desenhaCobra :: Cobra -> Picture
desenhaCobra cobra = Translate x y $ Color green $ circleSolid 20
  where
    (x, y) = fromInts cobra

desenhaMaçã :: Maçã -> Picture
desenhaMaçã maçã = Translate x y $ Color red $ circleSolid 10
  where
    (x, y) = fromInts maçã

evento :: Event -> Estado -> Estado
evento (EventKey (SpecialKey KeyUp) Down _ _) (Jogo (x, y) maçãs) =
  Jogo (x, y + 10) maçãs
evento (EventKey (SpecialKey KeyDown) Down _ _) (Jogo (x, y) maçãs) =
  Jogo (x, y - 10) maçãs
evento (EventKey (SpecialKey KeyLeft) Down _ _) (Jogo (x, y) maçãs) =
  Jogo (x - 10, y) maçãs
evento (EventKey (SpecialKey KeyRight) Down _ _) (Jogo (x, y) maçãs) =
  Jogo (x + 10, y) maçãs
evento _ e = e

animaJogo :: Jogo -> Jogada -> Jogo
animaJogo (Jogo (x, y) maçãs) Cima = (Jogo (x, y + 10) maçãs)
animaJogo (Jogo (x, y) maçãs) Baixo = (Jogo (x, y - 10) maçãs)
animaJogo (Jogo (x, y) maçãs) Direita = (Jogo (x + 10, y) maçãs)
animaJogo (Jogo (x, y) maçãs) Esquerda = (Jogo (x - 10, y) maçãs)
animaJogo (Jogo (x, y) maçãs) Parado = (Jogo (x, y) maçãs)

tempo :: Float -> Estado -> Estado
tempo _ e = e

fr :: Int
fr = 15

janela :: Display
janela = InWindow "Snake da Wish" (800, 800) (0, 0)

main =do
    -- rio <- loadBMP "rio.bmp"
    -- estrada <- loadBMP "estrada.bmp"
    -- relva <- loadBMP "relva.bmp"
    -- let images = [(Rio, rio), (Estrada, estrada), (Relva, relva)]
    -- let rio = lookup Rio images
    play janela white fr estadoInicial desenha evento tempo
