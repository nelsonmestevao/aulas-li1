module Spec where

import qualified Fact_Spec  as Fact
import qualified Fib_Spec   as Fib
import           Test.HUnit

main = runTestTTAndExit $ test [Fact.tests, Fib.tests]
