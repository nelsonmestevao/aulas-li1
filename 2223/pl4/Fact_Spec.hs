module Fact_Spec where

import           Fact
import           Test.HUnit

tests =
  TestLabel
    "Fact"
    $ test
      [ "fact 0" ~: 1 ~=? fact 0,
        "fact 6" ~: 720 ~=? fact 6,
        "fact 5" ~: 120 ~=? fact 5
      ]
